#!/home/isit/anaconda2/bin/python

'''
Runs modified fast R-CNN algorithm for detection.
This script is to run the SCENARIO 1, which is:
1. Generate test data for a specific imaging condition
2. Train incrementally (in a loop) for the single chosen imaging condition.
3. Test using the generated in step 1.

Incremental training is when we initialize training with the model obtained from previous loop.
'''

from global_vars import *
import os
from generator import *
from trainer import *
from incrementor import *
from detector import *
from evaluator import *
import scipy.io as sio
import shutil


if G_IS_LOGGING:
	logFile = ' >> output.log'
else:
	logFile = ' > /dev/null 2>&1'


# some initialization
completed = False
accuracies = []
easy_accuracies = []
medium_accuracies = []
hard_accuracies = []
no_map = []
easy_map = []
medium_map = []
hard_map = []
delOrder = ['no', 'easy', 'medium', 'hard']
# A common incrementor object to use in our loop
commonInc = Incrementor(delOrder, logFile)
commonInc.curr_del = 'no'
doGenerateTestData = True

###############################################################################
## Get the latest trained model to be used
###############################################################################
def get_latest_model():
	os.chdir(G_DETECTOR_PATH + '/output/default/train/')
	cmd = 'ls -t1 | head -n 1'
	proc = subprocess.Popen(cmd, stdout=subprocess.PIPE,shell=True)
	(op,err) = proc.communicate()
	op = op.rstrip('\n')
	op = G_DETECTOR_PATH + '/output/default/train/' + op
	return op


# iteratively train and test 
k = 1 # to store the models at each iteration
while not completed:
	# generate training data
	mode = 'train'
	isInLoop = True
	totalMeshes = G_NUM_OBJECTS * G_LOOP_MESHES_PER_OBJ
	currGen = Generator(totalMeshes, mode, commonInc.curr_del, isInLoop, logFile)
	currGen.generate_data()
	currGen.generate_proposals()
	del currGen

	# train using the training data
	# latestModel = get_latest_model()
	latestModel = '/home/isit/workspace/fast-rcnn/output/default/train/CaffeNet.v2.caffemodel'
	print ("Going to TRAIN with model initialization %s " % latestModel)
	tr = Trainer(latestModel, logFile)
	latestModel = tr.train(G_LOOP_TRAINING_ITERATIONS * k)
	newFile = latestModel.strip('.caffemodel') + '_' + str(k) + '.caffemodel'
	print ("Latest model is: %s trained in: %s iterations" % (newFile, k*G_LOOP_TRAINING_ITERATIONS))
	shutil.copy(latestModel,newFile)
	del latestModel
	del tr
	k += 1

	# generate test data
	if doGenerateTestData:
		mode = 'test'
		isInLoop = True
		print ("\nGenerating test data for delta: ", commonInc.curr_del, "\n")
		totalMeshes =  G_TEST_M
		currGen = Generator(totalMeshes, mode, commonInc.curr_del, isInLoop, k, logFile)
		currGen.generate_data(False)


	# Detect using the newly trained model
	testPrototxtFile = G_DETECTOR_PATH + '/models/DefoObject/' + 'test.prototxt'
	testProposalsFile = '/home/isit/workspace/DefoObjInWild/dataset/test_proposals.mat'
	latestModel = get_latest_model()
	print ("Going to init Detector using the model: ", latestModel)
	currDetector = Detector(latestModel, testPrototxtFile, testProposalsFile, logFile)
	predictResMatFile = currDetector.detect()
	gtMatFile = currDetector.get_gt_data()
	del latestModel
	del currDetector

	# Evaluate detection
	currEval = Evaluator(gtMatFile, predictResMatFile, logFile)
	status = currEval.evaluate()

	oldDelta = commonInc.curr_del
	completed, temp_accuracy, meanAvPr = commonInc.check_results()
	print ("ACCURACY in the iteration is: %s with difficulty: %s " % (temp_accuracy, commonInc.curr_del))

	# we generate new test data if the difficulty level changes
	if not oldDelta == commonInc.curr_del:
		print ("Setting generate test data to True")
		doGenerateTestData = True
	else:
		doGenerateTestData = False

	# append to right array for storing
	if commonInc.curr_del == 'no':
		accuracies.append(temp_accuracy)
		no_map.append(meanAvPr)
	if commonInc.curr_del == 'easy':
		easy_accuracies.append(temp_accuracy)
		easy_map.append(meanAvPr)
	if commonInc.curr_del == 'medium':
		medium_accuracies.append(temp_accuracy)
		medium_map.append(meanAvPr)
	if commonInc.curr_del == 'hard':
		hard_accuracies.append(temp_accuracy)
		hard_map.append(meanAvPr)

	# store the computed accuracies
	sio.savemat('no_accuracy.mat',{'accuracy' : accuracies})
	sio.savemat('easy_accuracy.mat',{'accuracy' : easy_accuracies})
	sio.savemat('medium_accuracy.mat',{'accuracy' : medium_accuracies})
	sio.savemat('hard_accuracy.mat',{'accuracy' : hard_accuracies})
	# store the mean average precisions
	sio.savemat('no_map.mat', {'map' : no_map})
	sio.savemat('easy_map.mat', {'map' : easy_map})
	sio.savemat('medium_map.mat', {'map' : medium_map})
	sio.savemat('hard_map.mat', {'map' : hard_map})