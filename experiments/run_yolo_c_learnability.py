from detector import *
import scipy.io as sio
import shutil
import matlab.engine
from utils import save_gt_data, get_latest_model, clean_for_learnability, evaluate_detections

test_mode = 'increment'  # or standard | increment
train_mode = 'increment'  # or discard  | increment
# conditions and difficulties considered
delOrder = ['easy', 'medium', 'hard']
# imgConditions = ['po', 'de', 'eo', 'fv','fb','mb']
imgConditions = ['li']
logFile = ''
GT_FILES_PATH = '/home/shrinivasan/work/defoobjinwild/dataset/gt_bboxes.mat'
iter_limit = 12

# start a matlab session to be used
eng = matlab.engine.start_matlab()

for cond in imgConditions:
	# clean all test data and previously trained models
	clean_for_learnability()
	# generate test set for all difficulty levels
	for currDel in delOrder:
		mode = 'test'
		isInLoop = True
		k = 1
		print ("\nGenerating test data for condition: %s with difficulty: %s \n" % (cond, currDel))
		totalMeshes = G_IMGS_PER_TEST_CONDITION * G_NUM_OBJECTS
		currGen = Generator(totalMeshes, mode, currDel, isInLoop, k, cond, logFile)
		currGen.generate_data(False)  # true will delete existing data
	GT_FILES_PATH = save_gt_data()

	# train for each difficulty
	for currDel in delOrder:
		k = 1
		stop = False
		mAPScores = []
		while k < iter_limit:
			mode = 'train'
			isInLoop = True
			totalMeshes = G_LOOP_MESHES_PER_OBJ * G_NUM_OBJECTS
			currGen = Generator(totalMeshes, mode, currDel, isInLoop, k, cond, logFile)
			currGen.generate_data(True)
			latestModel = get_latest_model()
			tr = Trainer(latestModel, logFile)
			latestModel = tr.train(G_LOOP_TRAINING_ITERATIONS)
			newFile = latestModel.strip('.weights') + '_' + str(k) + '.weights'
			shutil.copy(latestModel, newFile)

			latestModel = get_latest_model()
			currDetector = Detector(latestModel, '', '', k, cond, currDel, logFile)
			if currDetector.detect():
				# specific to YOLO_C
				predictResMatFile = G_DETECTOR_PATH + '/results/test.txt'

			print('Going to evaluate detections with: ', GT_FILES_PATH, predictResMatFile)
			# evaluate and keep appending the mAP scores to a list
			mAPScore = evaluate_detections(eng, GT_FILES_PATH, predictResMatFile)
			mAPScores.append(mAPScore)
			k += 1
			# store the MAP scores for plotting
			map_file = G_PROJECT_ROOT + '/dataset/predictions/map_scores_' + cond + '_' + currDel + '.mat'
			sio.savemat(map_file, {'map': mAPScores})
