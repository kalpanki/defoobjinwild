from detector import *
import scipy.io as sio
import shutil
import matlab.engine
from utils import save_gt_data,get_latest_model,stop_criteria, evaluate_detections

test_mode = 'standard' # or standard | increment
train_mode = 'discard' # or discard  | increment
# conditions and difficulties considered
# delOrder = ['hard']
delOrder = ['easy', 'medium', 'hard']
imgConditions = ['fb', 'mb', 'po', 'de', 'eo', 'fv','li']
logFile = ''
GT_FILES_PATH = '/home/shrinivasan/work/defoobjinwild/dataset/gt_bboxes.mat'

# start a matlab session to be used
eng = matlab.engine.start_matlab()

# generate test datset
if test_mode == 'standard':
	for cond in imgConditions:
		for delta in ['easy','medium','hard']:
			mode = 'test'
			isInLoop = True
			k = 1
			print ("\nGenerating test data for condition: %s with difficulty: %s \n" % (cond, delta))
			totalMeshes = G_IMGS_PER_TEST_CONDITION * G_NUM_OBJECTS
			currGen = Generator(totalMeshes, mode, delta, isInLoop, k, cond, logFile)
			currGen.generate_data(False) # true will delete existing data
	GT_FILES_PATH = save_gt_data()


for cond in imgConditions:
	for currDel in delOrder:
		stop = False
		# to store the models at each iteration
		k = 1
		print ("Running training loop for Imaging Condition: %s difficulty: %s iteration: %s " % (cond, currDel, k))
		if test_mode == 'increment':
			mode = 'test'
			isInLoop = True
			print ("\nGenerating test data for condition: %s with difficulty: %s \n" % (cond, currDel))
			totalMeshes = G_IMGS_PER_TEST_CONDITION * G_NUM_OBJECTS
			currGen = Generator(totalMeshes, mode, currDel, isInLoop, k, cond, logFile)
			currGen.generate_data(False)  # delete previously existing data
			GT_FILES_PATH = save_gt_data()
		mAPScores = []
		while not stop:
			# generate the training data
			mode = 'train'
			isInLoop = True
			totalMeshes = G_LOOP_MESHES_PER_OBJ * G_NUM_OBJECTS
			currGen = Generator(totalMeshes, mode, currDel, isInLoop, k, cond, logFile)
			if train_mode == 'increment':
				currGen.generate_data(False)
			else:  # train_mode is 'discard'
				# if True, delete existing
				currGen.generate_data(True)

			# train with the training data
			latestModel = get_latest_model()
			print ("Going to TRAIN with model initialization %s " % latestModel)
			tr = Trainer(latestModel, logFile)
			# no. of iterations is set in darknets/cfg/mildnets.cfg in max_batches
			latestModel = tr.train(G_LOOP_TRAINING_ITERATIONS)
			newFile = latestModel.strip('.weights') + '_' + str(k) + '.weights'
			print ("Latest model is: %s " % newFile)
			shutil.copy(latestModel, newFile)
			del latestModel
			del tr

			latestModel = get_latest_model()
			print ("Going to init Detector using the model: ", latestModel)
			currDetector = Detector(latestModel,'', '', k, cond, currDel, logFile)
			# TODO: modify the detector
			if currDetector.detect():
				if G_DETECTOR == 'YOLO_C':
					predictResMatFile = G_DETECTOR_PATH + '/results/test.txt'
				else:
					predictResMatFile = G_PROJECT_ROOT + '/dataset/predictions/' +  'pr_bboxes_' + cond + '_' + currDel + '_' + str(k) + '.mat'
			print('Going to evaluate detections with: ', GT_FILES_PATH, predictResMatFile)
			mAPScore = evaluate_detections(eng, GT_FILES_PATH, predictResMatFile)
			mAPScores.append(mAPScore)
			print('MAP scores are: ', mAPScores)
			# stop if stopping criteria is met
			stop = stop_criteria(mAPScores)
			k += 1
			# store the MAP scores for plotting
			map_file = G_PROJECT_ROOT + '/dataset/predictions/map_scores_' + cond + '_' + currDel + '.mat'
			sio.savemat(map_file, {'map': mAPScores})

eng.quit()
