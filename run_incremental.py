#!/home/isit/anaconda2/bin/python

'''
Runs modified fast R-CNN algorithm for detection.
Runs incremental training for each condition and difficulty.

'''


from global_vars import *
import os
from generator import *
from trainer import *
from incrementor import *
from detector import *
from evaluator import *
import scipy.io as sio
import shutil
import numpy as np

# some initializations
completed = False
fb_map = []
mb_map = []
po_map = []
de_map = []
eo_map = []
fv_map = []
logFile = ' > output.log'
delOrder = ['easy','medium', 'hard']
# A common incrementor object to use in our loop
commonInc = Incrementor(delOrder, logFile)
commonInc.curr_del = 'easy'
doGenerateTestData = True
# to store the models at each iteration
k = 1 
imgConditions = ['de', 'eo', 'fv']
# imgConditions = ['eo', 'fv', 'mb']

# useful function
def get_latest_model():
	os.chdir(G_DETECTOR_PATH + '/output/default/train/')
	cmd = 'ls -t1 | head -n 1'
	proc = subprocess.Popen(cmd, stdout=subprocess.PIPE,shell=True)
	(op,err) = proc.communicate()
	op = op.rstrip('\n')
	op = G_DETECTOR_PATH + '/output/default/train/' + op
	return op

# should we stop and move on to next level of diff or cond
def stop_criteria(cond, delta):
	thresh = 0.1
	# read the mat file corresponding to our difficulty
	mat = sio.loadmat('map_%s_%s.mat' % (cond,delta))
	mat = mat['map']
	mat = np.squeeze(mat)
	if np.size(mat) < 3:
		return False
	# check criteria
	M = 7; N = 3
	# check for threshold values
	m1 = max(mat[-M:]) ; m2 = max(mat[-M-N:-M])
	print ("\n The absolute difference btw MAX values is: %f \n" % abs(m1 - m2))
	if abs(m1 - m2) < thresh:
		return True
	return False

# generate test data for each condition
mode = 'test'
isInLoop = True
for cond in imgConditions:
	for currDel in delOrder:
		commonInc.curr_del = currDel
		print ("\nGenerating test data for condition: %s with difficulty: %s \n" % (cond, currDel))
		totalMeshes = G_LOOP_MESHES_PER_OBJ 
		currGen = Generator(totalMeshes, mode, commonInc.curr_del, isInLoop, k, cond, logFile)
		currGen.generate_data(False)
currGen.generate_proposals()

# variables to store our result
map_fb_easy = []
map_mb_easy = []
map_po_easy = []
map_de_easy = []
map_eo_easy = []
map_fv_easy = []
map_fb_medium = []
map_mb_medium = []
map_po_medium = []
map_de_medium = []
map_eo_medium = []
map_fv_medium = []
map_fb_hard = []
map_mb_hard = []
map_po_hard = []
map_de_hard = []
map_eo_hard = []
map_fv_hard = []

acc_fb_easy = []
acc_mb_easy = []
acc_po_easy = []
acc_de_easy = []
acc_eo_easy = []
acc_fv_easy = []
acc_fb_medium = []
acc_mb_medium = []
acc_po_medium = []
acc_de_medium = []
acc_eo_medium = []
acc_fv_medium = []
acc_fb_hard = []
acc_mb_hard = []
acc_po_hard = []
acc_de_hard = []
acc_eo_hard = []
acc_fv_hard = []
stop = False

# iterate each cond and diff
for cond in imgConditions:
	for currDel in delOrder:
		stop = False
		print ("Running Imaging Condition: %s with difficulty: %s in iteration: %s " % (cond, currDel, k))
		commonInc.curr_del = currDel

		# dummy stop criteria for now: 10 iterations
		k = 0
		# for k in range(10):
		while not stop:
			# generate training data
			mode = 'train'
			isInLoop = True
			totalMeshes = G_LOOP_MESHES_PER_OBJ
			currGen = Generator(totalMeshes, mode, commonInc.curr_del, isInLoop, k, cond, logFile)
			currGen.generate_data(True)
			currGen.generate_proposals()
			currGen = 0
			# train using the training data
			latestModel = get_latest_model()
			print ("Going to TRAIN with model initialization %s " % latestModel)
			tr = Trainer(latestModel, logFile)
			latestModel = tr.train(G_LOOP_TRAINING_ITERATIONS)
			newFile = latestModel.strip('.caffemodel') + '_' + str(k) + '.caffemodel'
			print ("Latest model is: %s " % newFile)
			shutil.copy(latestModel,newFile)
			del latestModel
			del tr
			k += 1

			# Detect using the newly trained model
			testPrototxtFile = G_DETECTOR_PATH + '/models/DefoObject/' + 'test.prototxt'
			testProposalsFile = '/home/isit/workspace/DefoObjInWild/dataset/test_proposals.mat'
			latestModel = get_latest_model()
			print ("Going to init Detector using the model: ", latestModel)
			currDetector = Detector(latestModel, testPrototxtFile, testProposalsFile, logFile)
			predictResMatFile = currDetector.detect()
			gtMatFile = currDetector.get_gt_data()
			# Evaluate detection
			currEval = Evaluator(gtMatFile, predictResMatFile, logFile)
			status = currEval.evaluate()
			completed, accuracy, meanAvPr = commonInc.check_results()
			print ("MEAN IOU in the iteration is: %s with difficulty: %s " % (meanAvPr, commonInc.curr_del))
			# save the result
			globals()['map_%s_%s' % (cond,currDel)].append(meanAvPr)
			globals()['acc_%s_%s' % (cond,currDel)].append(accuracy)

			# save everything
			sio.savemat('map_fb_easy.mat', {'map' : map_fb_easy})
			sio.savemat('map_mb_easy.mat', {'map' : map_mb_easy})
			sio.savemat('map_po_easy.mat', {'map' : map_po_easy})
			sio.savemat('map_eo_easy.mat', {'map' : map_eo_easy})
			sio.savemat('map_de_easy.mat', {'map' : map_de_easy})
			sio.savemat('map_fv_easy.mat', {'map' : map_fv_easy})
			sio.savemat('map_fb_medium.mat', {'map' : map_fb_medium})
			sio.savemat('map_mb_medium.mat', {'map' : map_mb_medium})
			sio.savemat('map_po_medium.mat', {'map' : map_po_medium})
			sio.savemat('map_eo_medium.mat', {'map' : map_eo_medium})
			sio.savemat('map_de_medium.mat', {'map' : map_de_medium})
			sio.savemat('map_fv_medium.mat', {'map' : map_fv_medium})
			sio.savemat('map_fb_hard.mat', {'map' : map_fb_hard})
			sio.savemat('map_mb_hard.mat', {'map' : map_mb_hard})
			sio.savemat('map_po_hard.mat', {'map' : map_po_hard})
			sio.savemat('map_eo_hard.mat', {'map' : map_eo_hard})
			sio.savemat('map_de_hard.mat', {'map' : map_de_hard})
			sio.savemat('map_fv_hard.mat', {'map' : map_fv_hard})

			sio.savemat('acc_fb_easy.mat', {'map' : acc_fb_easy})
			sio.savemat('acc_mb_easy.mat', {'map' : acc_mb_easy})
			sio.savemat('acc_po_easy.mat', {'map' : acc_po_easy})
			sio.savemat('acc_eo_easy.mat', {'map' : acc_eo_easy})
			sio.savemat('acc_de_easy.mat', {'map' : acc_de_easy})
			sio.savemat('acc_fv_easy.mat', {'map' : acc_fv_easy})
			sio.savemat('acc_fb_medium.mat', {'map' : acc_fb_medium})
			sio.savemat('acc_mb_medium.mat', {'map' : acc_mb_medium})
			sio.savemat('acc_po_medium.mat', {'map' : acc_po_medium})
			sio.savemat('acc_eo_medium.mat', {'map' : acc_eo_medium})
			sio.savemat('acc_de_medium.mat', {'map' : acc_de_medium})
			sio.savemat('acc_fv_medium.mat', {'map' : acc_fv_medium})
			sio.savemat('acc_fb_hard.mat', {'map' : acc_fb_hard})
			sio.savemat('acc_mb_hard.mat', {'map' : acc_mb_hard})
			sio.savemat('acc_po_hard.mat', {'map' : acc_po_hard})
			sio.savemat('acc_eo_hard.mat', {'map' : acc_eo_hard})
			sio.savemat('acc_de_hard.mat', {'map' : acc_de_hard})
			sio.savemat('acc_fv_hard.mat', {'map' : acc_fv_hard})

			stop = stop_criteria(cond,currDel)
