#!/home/isit/anaconda2/bin/python

from global_vars import *
import os
from generator import *
from trainer import *
from incrementor import *
from detector import *
from evaluator import *
import scipy.io as sio
import shutil
import numpy as np

# some initializations
logFile = ' > output.log'
delOrder = ['medium', 'hard']
# A common incrementor object to use in our loop
commonInc = Incrementor(delOrder, logFile)
commonInc.curr_del = 'no'
# imgConditions = ['mb', 'po', 'de', 'eo', 'fv']
imgConditions = ['no']

# useful function
def get_latest_model():
	os.chdir(G_DETECTOR_PATH + '/output/default/train/')
	cmd = 'ls -t1 | head -n 1'
	proc = subprocess.Popen(cmd, stdout=subprocess.PIPE,shell=True)
	(op,err) = proc.communicate()
	op = op.rstrip('\n')
	op = G_DETECTOR_PATH + '/output/default/train/' + op
	return op

def stop_criteria(cond, delta):
	thresh = 0.02
	# read the mat file corresponding to our difficulty
	mat = sio.loadmat('map_%s_%s.mat' % (cond,delta))
	mat = mat['map']
	mat = np.squeeze(mat)
	if np.size(mat) < 3:
		return False
	# check criteria
	M = 7; N = 3
	# check for threshold values
	m1 = max(mat[-M:]) ; m2 = max(mat[-M-N:-M])
	print ("\n The absolute difference btw MAX values is: %f \n" % abs(m1 - m2))
	if abs(m1 - m2) < thresh:
		return True
	return False

def clean_test_directories():
	os.chdir(G_PROJECT_ROOT + '/dataset/Annotations')
	filelist = [ f for f in os.listdir(".") if f.endswith(".jpg")]
	if len(filelist) > 0:
		for f in filelist:
			os.remove(f)
		print ("Deleted files in TestImages folder")

	os.chdir(G_PROJECT_ROOT + '/dataset/TestAnnotations')
	filelist = [ f for f in os.listdir(".") if f.endswith(".xml")]
	if len(filelist) > 0:
		for f in filelist:
			os.remove(f)
		print ("Deleted files in Annotations folder")

	os.chdir(G_PROJECT_ROOT + '/dataset/TestImages')
	filelist = [ f for f in os.listdir(".") if f.endswith(".jpg")]
	if len(filelist) > 0:
		for f in filelist:
			os.remove(f)
		print ("Deleted files in Images folder")

	os.chdir(G_PROJECT_ROOT + '/dataset/ImageSets')
	filelist = [ f for f in os.listdir(".") if f.endswith("test.txt")]
	if len(filelist) > 0:
		for f in filelist:
			os.remove(f)
		print ("Deleted files in ImageSets folder")

	os.chdir(G_PROJECT_ROOT + '/dataset')
	filelist = [ f for f in os.listdir(".") if f.endswith("test_proposals.mat")]
	if len(filelist) > 0:
		for f in filelist:
			os.remove(f)
		print ("Deleted test and train mat files")

def save_gt_data(cond, difficulty = 'all'):
		test_set_file = '/home/isit/workspace/DefoObjInWild/dataset/ImageSets/test.txt'
		with open(test_set_file) as f:
			lines = f.readlines()
		# to store ground truth
		gt_bboxes = np.zeros((len(lines),5))

		for l in range(len(lines)):
			line = lines[l]
			line = line.strip('\n')
			print ("line is: ", line)
			xml_file = '/home/isit/workspace/DefoObjInWild/dataset/' + 'TestAnnotations/' + line + '.xml'
			tree = ET.parse(xml_file)
			root = tree.getroot()
			# get class index
			class_idx = (root[1][0].text).strip('object_')
			gt_bboxes[l,-1] = int(class_idx)
			# get the box locations
			for i in range(4):
				gt_bboxes[l,i] = int(root[1][1][i].text)
				print ("in for loop: ", gt_bboxes[l,i])

		# print ("GT BOXES ARE: ", gt_bboxes)

		gt_dest = '/home/isit/workspace/DefoObjInWild/dataset' + '/gt_bboxes_' + cond + '_' + difficulty + '.mat'
		sio.savemat(gt_dest, {'gt_bboxes' : gt_bboxes})

# ------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------
# The below is to run incremental training for:
# Scenario 1: Test set with all imaging conditions
# At this point, test data is assumed to be present. So no need to generate test data.
# ------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------
for cond in imgConditions:
	for currDel in delOrder:
		stop = False
		# to store the models at each iteration
		k = 1 
		while not stop:
			print ("Running Imaging Condition: %s with difficulty: %s in iteration: %s " % (cond, currDel, k))
			# generate training data
			mode = 'train'
			isInLoop = True
			totalMeshes = G_LOOP_MESHES_PER_OBJ
			currGen = Generator(totalMeshes, mode, commonInc.curr_del, isInLoop, k, cond, logFile)
			currGen.generate_data(True) # if True, delete existing
			currGen.generate_proposals()
			currGen = 0
			# train with the training data
			latestModel = get_latest_model()
			print ("Going to TRAIN with model initialization %s " % latestModel)
			tr = Trainer(latestModel, logFile)
			latestModel = tr.train(G_LOOP_TRAINING_ITERATIONS)
			newFile = latestModel.strip('.caffemodel') + '_' + str(k) + '.caffemodel'
			print ("Latest model is: %s " % newFile)
			shutil.copy(latestModel,newFile)
			del latestModel
			del tr
			# test using the test set
			testPrototxtFile = G_DETECTOR_PATH + '/models/DefoObject/' + 'test.prototxt'
			testProposalsFile = '/home/isit/workspace/DefoObjInWild/dataset/test_proposals.mat'
			latestModel = get_latest_model()
			print ("Going to init Detector using the model: ", latestModel)
			currDetector = Detector(latestModel, testPrototxtFile, testProposalsFile, k, cond, currDel, logFile)
			predictResMatFile = currDetector.detect()
			k += 1
			if k == 11:
				stop = True

# ------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------
# The below is to run incremental training for:
# Scenario 2: A test set per Imaging condition - 
# 30 test images per condition, 25 training images per condition per iter, k
# The below method was run by generating 100 test images per condition and
# 25 training images per iteration, k, of training increment loop.
# ------------------------------------------------------------------------------------
# for cond in imgConditions:
# 	# generate test data for each difficulty in the condition
# 	clean_test_directories()
# 	for currDel in delOrder:
# 		mode = 'test'
# 		isInLoop = True
# 		k = 1
# 		commonInc.curr_del = currDel
# 		print ("\nGenerating test data for condition: %s with difficulty: %s \n" % (cond, currDel))
# 		totalMeshes = G_IMGS_PER_TEST_CONDITION
# 		currGen = Generator(totalMeshes, mode, currDel, isInLoop, k, cond, logFile)
# 		currGen.generate_data(False) # true will delete existing data
# 	currGen.generate_proposals()
# 	save_gt_data(cond)
# 	currGen = 0
# 	# initialize always from caffeNet. Delete others
# 	try:
# 		os.system("rm /home/isit/workspace/fast-rcnn/output/default/train/*_iter_*.caffemodel")
# 	except:
# 		print ("Unable to delete caffe models")

# 	for currDel in delOrder:
# 		stop = False
# 		# to store the models at each iteration
# 		k = 1 
# 		while not stop:
# 			# generate training data 
# 			mode = 'train'
# 			isInLoop = True
# 			totalMeshes = G_LOOP_MESHES_PER_OBJ
# 			currGen = Generator(totalMeshes, mode, currDel, isInLoop, k, cond, logFile)
# 			currGen.generate_data(True) # if True, delete existing
# 			currGen.generate_proposals()
# 			currGen = 0
# 			# train with the training data
# 			latestModel = get_latest_model()
# 			print ("Going to TRAIN with model initialization %s " % latestModel)
# 			tr = Trainer(latestModel, logFile)
# 			latestModel = tr.train(G_LOOP_TRAINING_ITERATIONS)
# 			newFile = latestModel.strip('.caffemodel') + '_' + str(k) + '.caffemodel'
# 			print ("Latest model is: %s " % newFile)
# 			shutil.copy(latestModel,newFile)
# 			del latestModel
# 			del tr
# 			# test using the test set
# 			testPrototxtFile = G_DETECTOR_PATH + '/models/DefoObject/' + 'test.prototxt'
# 			testProposalsFile = '/home/isit/workspace/DefoObjInWild/dataset/test_proposals.mat'
# 			latestModel = get_latest_model()
# 			print ("Going to init Detector using the model: ", latestModel)
# 			currDetector = Detector(latestModel, testPrototxtFile, testProposalsFile, k, cond, currDel, logFile)
# 			# gtMatFile = currDetector.get_gt_data()
# 			predictResMatFile = currDetector.detect()
# 			k += 1
# 			if k == 11:
# 				stop = True


# ------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------
# The below is to run incremental training for:
# Scenario 3: Growing Training set with Imaging Conditions
# ------------------------------------------------------------------------------------
# for cond in imgConditions:
# 	for currDel in delOrder:
# 		stop = False
# 		# to store the models at each iteration
# 		k = 1 
# 		while not stop:
# 			print ("Running Imaging Condition: %s with difficulty: %s in iteration: %s " % (cond, currDel, k))
# 			# generate training data
# 			mode = 'train'
# 			isInLoop = True
# 			totalMeshes = G_LOOP_MESHES_PER_OBJ
# 			currGen = Generator(totalMeshes, mode, commonInc.curr_del, isInLoop, k, cond, logFile)
# 			currGen.generate_data(False) # if True, delete existing
# 			currGen.generate_proposals()
# 			currGen = 0
# 			# train with the training data
# 			latestModel = get_latest_model()
# 			print ("Going to TRAIN with model initialization %s " % latestModel)
# 			tr = Trainer(latestModel, logFile)
# 			latestModel = tr.train(G_LOOP_TRAINING_ITERATIONS)
# 			newFile = latestModel.strip('.caffemodel') + '_' + str(k) + '.caffemodel'
# 			print ("Latest model is: %s " % newFile)
# 			shutil.copy(latestModel,newFile)
# 			del latestModel
# 			del tr
# 			# test using the test set
# 			testPrototxtFile = G_DETECTOR_PATH + '/models/DefoObject/' + 'test.prototxt'
# 			testProposalsFile = '/home/isit/workspace/DefoObjInWild/dataset/test_proposals.mat'
# 			latestModel = get_latest_model()
# 			print ("Going to init Detector using the model: ", latestModel)
# 			currDetector = Detector(latestModel, testPrototxtFile, testProposalsFile, k, cond, currDel, logFile)
# 			predictResMatFile = currDetector.detect()
# 			k += 1
# 			if k == 11:
# 				stop = True
				