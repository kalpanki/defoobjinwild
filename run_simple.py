#!/home/isit/anaconda2/bin/python

'''
Runs modified fast R-CNN algorithm for detection.
Adding conditions one after the other:
1. We iterate over a list of imaging conditions
2. For each difficulty in that imaging condition, we 
generate test images, Ste <= Ste(condition) U Ste
3. Iteratively, For each difficulty in that imaging condition, we 
generate train images, Str <= Str(condition) U Str
and train till we reach a threshold

Imaging conditions considered:
Focus Blur, Motion Blur, Pose Change, Deformation, Self Occlution, 
External Occlution, Field of View/Scale change.
'''


from global_vars import *
import os
from generator import *
from trainer import *
from incrementor import *
from detector import *
from evaluator import *
import scipy.io as sio
import shutil

logFile = ' > output.log'
delOrder = ['no']
# A common incrementor object to use in our loop
commonInc = Incrementor(delOrder, logFile)
commonInc.curr_del = 'no'
k = 1
cond = ['no']

# generate training data
# 2000 train images
# 30,000 iterations
mode = 'train'
isInLoop = False
# 3000 images per class
totalMeshes = 1 * G_MESHES_PER_OBJ
currGen = Generator(totalMeshes, mode, commonInc.curr_del, isInLoop, k, cond, logFile)
currGen.generate_data(False)
currGen.generate_proposals()
del currGen

# train using generated data
# imagenet initialization
initModel = '/home/isit/workspace/fast-rcnn/data/imagenet_models/CaffeNet.v2.caffemodel'
print ("Going to TRAIN with model initialization %s " % latestModel)
tr = Trainer(initModel, logFile)
tr.train(30000)

