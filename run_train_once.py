#!/home/isit/anaconda2/bin/python

'''
Runs modified fast R-CNN algorithm for detection.
The approach is: 
1. Generate test data for a all possible imaging conditions.
2. Train in one go by generating data for all imaging conditions
3. Test using the generated in step 1.
'''


from global_vars import *
import os
from generator import *
from trainer import *
from incrementor import *
from detector import *
from evaluator import *
import scipy.io as sio
import shutil

logFile = ' >> output.log'

# some initializations
completed = False
accuracies = []
easy_accuracies = []
medium_accuracies = []
hard_accuracies = []
no_map = []
delOrder = ['no', 'easy', 'medium', 'hard']
# A common incrementor object to use in our loop
commonInc = Incrementor(delOrder, logFile)
commonInc.curr_del = 'no'
doInitTraining = True
doGenerateTestData = False

# generate training data
mode = 'train'
isInLoop = True
# 3000 images per class
totalMeshes = 1 * 3000
currGen = Generator(totalMeshes, mode, commonInc.curr_del, isInLoop, logFile)
currGen.generate_data()
currGen.generate_proposals()
del currGen

# train using generated data
# imagenet initialization
initModel = '/home/isit/workspace/fast-rcnn/data/imagenet_models/CaffeNet.v2.caffemodel'
print ("Going to TRAIN with model initialization %s " % latestModel)
tr = Trainer(initModel, logFile)
tr.train(60000)

# generate testing data
mode = 'test'
isInLoop = False
for i in range(len(delOrder)):
	print ("\nGenerating test data for delta: ", delOrder[i], "\n")
	imgs_per_test_cond = 100
	totalMeshes = imgs_per_test_cond 
	currGen = Generator(totalMeshes, mode, delOrder[i], isInLoop, logFile)
	currGen.generate_data(False)


# detect using the trained model
testPrototxtFile = G_DETECTOR_PATH + '/models/DefoObject/' + 'test.prototxt'
testProposalsFile = '/home/isit/workspace/DefoObjInWild/dataset/test_proposals.mat'
latestModel = get_latest_model()
currDetector = Detector(latestModel, testPrototxtFile, testProposalsFile, logFile)
predictResMatFile = currDetector.detect()
gtMatFile = currDetector.get_gt_data()

# evaluate and get scores
currEval = Evaluator(gtMatFile, predictResMatFile, logFile)
status = currEval.evaluate()
completed, temp_accuracy, meanAvPr = commonInc.check_results()
print ("ACCURACY in the iteration is: %s and mAP is: %s with difficulty: %s " % (temp_accuracy, meanAvPr, commonInc.curr_del))