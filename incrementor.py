#!/home/isit/anaconda2/bin/python

from __future__ import division
from global_vars import *
import os
import numpy as np 
from scipy.io import loadmat


class Incrementor:

	def __init__(self, del_order, log_file):
		self.log_file = log_file
		# 3 difficulties and their range
		self.del_order = del_order
		self.curr_del_idx = 0
		self.curr_del = del_order[self.curr_del_idx]

	def increment(self):
		self.curr_del_idx += 1
		self.curr_del = self.del_order[self.curr_del_idx]

	def check_results(self):
		'''
		check the results obtained from the IoU scores.
		Decide on the next level of difficulty to be used 
		bsed on the results.
		'''
		is_complete = False

		# read the IoU file
		iou_path = G_PROJECT_ROOT + '/dataset/iou_scores.mat'
		if not os.path.exists(iou_path):
			print "IoU file does not exist. Evaluate first."
			return
		iou = loadmat(iou_path)
		iou = iou['IoU']
		_m,_n = np.shape(iou)
		print (iou)
		# mean average precision
		meanAvPr = np.sum(iou) / _n
		# check how many are correct
		success = np.sum(iou > 0.5)
		num_test_images = G_NUM_OBJECTS * G_IMGS_PER_TEST_CONDITION * 18
		accuracy = success / num_test_images
		# if 95% of images are detected correctly, increase 
		# difficulty. Else, generate more data with same parameters
		if meanAvPr >= 0.90:
			# if we have succeeded in final level of difficulty, we are done.
			# otherwise, move the difficulty level
			if self.curr_del_idx == len(self.del_order)-1:
				is_complete = True
			else:
				self.increment()

		return is_complete,accuracy,meanAvPr



if __name__ == '__main__':

	if G_IS_LOGGING:
		log_file = ' >> output.log'
	else:
		log_file = ' > /dev/null 2>&1'

	currInc = Incrementor(log_file)
	print (currInc.get_rand_param('easy'))