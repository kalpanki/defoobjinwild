#!/usr/bin/python

from global_vars import *
import os
from generator import *
from trainer import *
from incrementor import *
from detector import *
import evaluator as eval
import scipy.io as sio
import shutil
import numpy as np
from utils import save_gt_data,get_latest_model,stop_criteria

# conditions and difficulties considered
delOrder = ['easy', 'medium', 'hard']
imgConditions = ['fb', 'mb', 'po', 'de', 'eo', 'fv']
logFile = '>> output.log'
GT_FILES_PATH = '/home/shrinivasan/work/defoobjinwild/dataset/gt_bboxes.mat'

# At this point it is assumed we have the standard dataset for testing.
# for training, we loop similar to incremental training. But we don't train
# in each increment. We train after all images are generated.

# generate training data for each cond, diff
mode = 'train'
isInLoop = True
for cond in imgConditions:
	for currDel in delOrder:
		totalMeshes = G_LOOP_MESHES_PER_OBJ * G_NUM_OBJECTS
		for k in range(10):
			currGen = Generator(totalMeshes, mode, currDel, isInLoop, k, cond, logFile)
			currGen.generate_data(False)
currGen.generate_proposals()

latestModel = '/home/shrinivasan/work/my_fast_rcnn/output/default/train/CaffeNet.v2.caffemodel'
testPrototxtFile = G_DETECTOR_PATH + '/models/DefoObject/' + 'test.prototxt'
testProposalsFile = '/home/shrinivasan/work/defoobjinwild/dataset/test_proposals.mat'
mAPScores = []

for i in range(30):
	tr = Trainer(latestModel, logFile)
	latestModel = '/home/shrinivasan/work/my_fast_rcnn/output/default/train/CaffeNet.v2.caffemodel'
	tr.train(i * 2000)
	newFile = latestModel.strip('.caffemodel') + '_' + str(i) + '.caffemodel'
	print ("Latest model is: %s " % newFile)
	shutil.copy(latestModel,newFile)
	currDetector = Detector(latestModel, testPrototxtFile, testProposalsFile, i, 'no', 'no', logFile)
	if currDetector.detect():
		predictResMatFile = G_PROJECT_ROOT + '/dataset/predictions/' +  'pr_bboxes_' + 'no' + '_' + 'no' + '_' + str(i) + '.mat'
	mAPScores.append(eval.evaluate_detections(GT_FILES_PATH, predictResMatFile))
	print mAPScores
	map_file = G_PROJECT_ROOT + '/dataset/predictions/map_scores_traditional.mat'
	sio.savemat(map_file, {'map': mAPScores})
