## Description
This repo is the implementation of the paper, [Model-based active learning to detect an isometric deformable object in the wild with a deep architecture](https://www.sciencedirect.com/science/article/pii/S1077314218300705) accepted in the Journal of Computer Vision and Image Understanding (CVIU). The paper is also available in arxiv [here](https://arxiv.org/pdf/1806.02850.pdf).

## Tools Needed
* Blender software for rendering
* Matlab for other operations
* Dollar toolbox: https://pdollar.github.io/toolbox/
* My EdgeBoxes toolbox available from: https://bitbucket.org/kalpanki/my_edge_boxes
* My fast R-CNN algorithm available at: https://bitbucket.org/kalpanki/my_fast_rcnn
* My modified version of Faster R-CNN python implementation available at: https://bitbucket.org/kalpanki/my_faster_rcnn

## Steps to setup
* Checkout the repository
* Make sure my version of Fast R-CNN is downloaded and compiled along with the caffe library that comes with it
* Make sure my version of py-faster-rcnn is downloaded and compiled with the caffe library included in it
* Modify the `global_vars.py` file to point to the right location of Fast R-CNN, blender root, matlab root
* Create the directories: ImageSets, temp_rendered, temp_meshes, Annotations and Images under the `dataset` directory in the `DefoObjInWild` module
* Create sym link to the edge boxes toolbox and `dataset` folder in `defoobjinwild` repo:
```
cd $MY_FAST_RCNN
ln -s /home/shrinivasan/work/my_edge_boxes edge_boxes
ln -s /home/shrinivasan/work/defoobjinwild/dataset
```

### Steps to customise Fast R-CNN for different objects
- Change hard coded values for object_* in defo_object.py
- Change the num_classes in the solver.
- Change the number of outputs num_outputs in the same file = num_classes*4
- rename the layer bbox_pred and cls_score to something else in the prototxt file such as: bbox_prod_do and cls_score_do

## Experiments
All files for running the proposed algorithms are in the `experiments` directory.

* The experiments for active learning can be run by:
```
python experiments/run_fast_rcnn_mildnets.py
python experiments/run_faster_rcnn_mildnets.py
```

* The experiments for learnability can be run by:
```
python experiments/run_fast_rcnn_learnability.py
python experiments/run_faster_rcnn_learnability.py
```
