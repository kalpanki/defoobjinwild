#!/home/isit/anaconda2/bin/python

import os

G_PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))
G_IS_LOGGING = True

# Set all the below paramters to customize the project
G_BLENDER_ROOT = '/usr/bin/blender'
G_MATLAB_ROOT = '/usr/bin/matlab'
# workspace for blender projects
G_BLENDER_WS = '/home/isit/workspace/blender_projects'
# The path for detector - Fast R-CNN in our case
G_DETECTOR_PATH = '/home/isit/workspace/fast-rcnn'
# path for test images
G_TEST_IMAGES = '/home/isit/workspace/DefoObjInWild/dataset/TestImages'
G_TEST_IMAGES_SET = '/home/isit/workspace/DefoObjInWild/dataset/ImageSets'

G_NUM_OBJECTS = 1
G_MESHES_PER_OBJ = 10
G_LOOP_MESHES_PER_OBJ = 30
# number of iterations to train the Fast R-CNN detector
G_LOOP_TRAINING_ITERATIONS = 1000
# Number of images to be generated per object (class) for each test cycle
G_TEST_M = 200
# passed to render_mesh.py for test mode
G_IMGS_PER_TEST_CONDITION = 30


'''
Experiment 1: Incremental initialization VS Training in one go
# For training in one go
G_LOOP_TRAINING_ITERATIONS = 30000
G_IMGS_PER_TEST_CONDITION = 500
G_NUM_OBJECTS = 1
G_LOOP_MESHES_PER_OBJ = 1500
G_IMGS_PER_TEST_CONDITION = 500

# For training incrementally
G_LOOP_TRAINING_ITERATIONS = 1000
G_NUM_OBJECTS = 1
G_LOOP_MESHES_PER_OBJ = 50
G_IMGS_PER_TEST_CONDITION = 500
'''

'''
Experiment 2: Growing trainig and test data (run_scenario_three.py)
SMALL DATA CASE:
G_NUM_OBJECTS = 1
G_MESHES_PER_OBJ = 200
G_LOOP_MESHES_PER_OBJ = 10
# number of iterations to train the Fast R-CNN detector
G_LOOP_TRAINING_ITERATIONS = 2000
# Number of images to be generated per object (class) for each test cycle
G_TEST_M = 200
# passed to render_mesh.py for test mode
G_IMGS_PER_TEST_CONDITION = 10
'''


'''
Experiment 3: Traditional case
SMALL DATA CASE:
G_NUM_OBJECTS = 1
G_MESHES_PER_OBJ = 200
G_LOOP_MESHES_PER_OBJ = 10
# number of iterations to train the Fast R-CNN detector
# 2000*18 = 36,000
G_LOOP_TRAINING_ITERATIONS = 36000
# Number of images to be generated per object (class) for each test cycle
G_TEST_M = 200
# passed to render_mesh.py for test mode
G_IMGS_PER_TEST_CONDITION = 10
'''