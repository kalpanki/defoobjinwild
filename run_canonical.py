#!/usr/bin/python

from global_vars import *
import os
from generator import *
from trainer import *
from incrementor import *
from detector import *
import evaluator as eval
import scipy.io as sio
import shutil
import numpy as np
from utils import save_gt_data,get_latest_model,stop_criteria


# conditions and difficulties considered
delOrder = ['no']
imgConditions = ['no']
logFile = '>> output.log'
GT_FILES_PATH = '/home/shrinivasan/work/defoobjinwild/dataset/gt_bboxes.mat'
num_iterations = 60000
isInLoop = True
mAPScores = []
mode = 'train'
# At this point it is assumed we have the standard dataset for testing.
# generate training data - canonical image:
# 1. increasing training images

def increasing_training_images():
    for k in range(10):
        totalMeshes = G_LOOP_MESHES_PER_OBJ * G_NUM_OBJECTS
        currGen = Generator(totalMeshes, mode, 'no', isInLoop, k, 'no', logFile)
        currGen.generate_data(False)
        currGen.generate_proposals()
        # always initialize with caffeNet
        latestModel = '/home/shrinivasan/work/my_fast_rcnn/output/default/train/CaffeNet.v2.caffemodel'
        tr = Trainer(latestModel, logFile)
        latestModel = tr.train(num_iterations)
        newFile = latestModel.strip('.caffemodel') + '_' + str(k) + '.caffemodel'
        shutil.copy(latestModel,newFile)
        del latestModel
        del tr
        testPrototxtFile = G_DETECTOR_PATH + '/models/DefoObject/' + 'test.prototxt'
        testProposalsFile = '/home/shrinivasan/work/defoobjinwild/dataset/test_proposals.mat'
        # get the latest trained model for testing
        latestModel = get_latest_model()
        print ("*************************************************************")
        print ("Going to run detector using the model: ", latestModel)
        print ("*************************************************************")
        currDetector = Detector(latestModel, testPrototxtFile, testProposalsFile, k, 'no', 'no', logFile)
        if currDetector.detect():
            predictResMatFile = G_PROJECT_ROOT + '/dataset/predictions/' +  'pr_bboxes_' + 'no' + '_' + 'no' + '_' + str(k) + '.mat'
        # evaluate and keep appending the mAP scores to a list
        mAPScores.append(eval.evaluate_detections(GT_FILES_PATH, predictResMatFile))
        print mAPScores
    map_file = G_PROJECT_ROOT + '/dataset/predictions/map_scores_no_no.mat'
    sio.savemat(map_file, {'map': mAPScores})



if __name__ == '__main__':
    increasing_training_images()
    # increasing_training_iterations()
