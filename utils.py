#!/usr/bin/python

import numpy as np
import os
import scipy.io as sio
from global_vars import *
import subprocess
import xml.etree.ElementTree as ET



def get_latest_model():
	os.chdir(G_DETECTOR_PATH + '/output/default/train/')
	cmd = 'ls -t1 | head -n 1'
	proc = subprocess.Popen(cmd, stdout=subprocess.PIPE,shell=True)
	(op,err) = proc.communicate()
	op = op.rstrip('\n')
	op = G_DETECTOR_PATH + '/output/default/train/' + op
	return op


def create_map(gt_file, cond, diff):
	'''
	Given the ground truth and prediction files, predicts
	calls the matlab file to calculate the Mean Avg Precision values
	and store them in the format 'map_<condition>_<difficulty>.mat'
	'''
	print "Creating Map Files for condition %s and difficulty %s", (gt_file, cond, diff)
	matlab_cmd = "addpath(genpath('%s')); plot_pr('%s', '%s', '%s');" % \
				((G_PROJECT_ROOT + '/utils'), gt_file, cond, diff)
	try:
		os.system('%s -nodisplay -r "try %s ; catch; end; quit;" %s' % \
			(G_MATLAB_ROOT, matlab_cmd, ''))
	except:
		sys.exit("Problem creating mean Average Precision (mAP)")
		return False
	return True


def save_gt_data():
		test_set_file = '/home/shrinivasan/work/defoobjinwild/dataset/ImageSets/test.txt'
		with open(test_set_file) as f:
			lines = f.readlines()
		# to store ground truth
		gt_bboxes = np.zeros((len(lines),5))

		for l in range(len(lines)):
			line = lines[l]
			line = line.strip('\n')
			print ("line is: ", line)
			xml_file = '/home/shrinivasan/work/defoobjinwild/dataset/' + 'TestAnnotations/' + line + '.xml'
			tree = ET.parse(xml_file)
			root = tree.getroot()
			# get class index
			class_idx = (root[1][0].text).strip('object_')
			gt_bboxes[l,-1] = int(class_idx)
			# get the box locations
			for i in range(4):
				gt_bboxes[l,i] = int(root[1][1][i].text)
				print ("in for loop: ", gt_bboxes[l,i])

		# print ("GT BOXES ARE: ", gt_bboxes)
		# gt_dest = '/home/shrinivasan/work/defoobjinwild/dataset' + '/gt_bboxes_' + cond + '_' + difficulty + '.mat'
		gt_dest = '/home/shrinivasan/work/defoobjinwild/dataset/gt_bboxes.mat'
		sio.savemat(gt_dest, {'gt_bboxes' : gt_bboxes})
		return gt_dest


def clean_test_directories():
	os.chdir(G_PROJECT_ROOT + '/dataset/TestAnnotations')
	filelist = [ f for f in os.listdir(".") if f.endswith(".xml")]
	if len(filelist) > 0:
		for f in filelist:
			os.remove(f)
		print ("Deleted files in TestAnnotations folder")

	os.chdir(G_PROJECT_ROOT + '/dataset/TestImages')
	filelist = [ f for f in os.listdir(".") if f.endswith(".jpg")]
	if len(filelist) > 0:
		for f in filelist:
			os.remove(f)
		print ("Deleted files in TestImages folder")

	os.chdir(G_PROJECT_ROOT + '/dataset/ImageSets')
	filelist = [ f for f in os.listdir(".") if f.endswith("test.txt")]
	if len(filelist) > 0:
		for f in filelist:
			os.remove(f)
		print ("Deleted test.txt file in ImageSets folder")

	os.chdir(G_PROJECT_ROOT + '/dataset')
	filelist = [ f for f in os.listdir(".") if f.endswith("test_proposals.mat")]
	if len(filelist) > 0:
		for f in filelist:
			os.remove(f)
		print ("Deleted test and train mat files")


def clean_for_learnability():
	# clean all test directories
	clean_test_directories()
	# clean all fast r-cnn models except caffe init model
	os.chdir(G_DETECTOR_PATH + '/output/default/train')
	filelist = [ f for f in os.listdir(".") if f.endswith(".caffemodel")]
	if len(filelist) > 0:
		for f in filelist:
			# hack - length of CaffeNet is 22. so avoid CaffeNet
			if len(f) != 22:
				os.remove(f)
		print ("Deleted all caffemodels")


def stop_criteria(map_scores):
	# check criteria
	N = 7; M = 3;
	thresh = 0.02
	if len(map_scores) > 10:
		Pn = map_scores[-1-M-N:-1-M]
		Pm = map_scores[-1-M:-1]
		mean_diff = abs(np.mean(Pn) - np.mean(Pm))
		print "Mean difference in stop criteria is:", mean_diff
		if (abs(np.mean(Pn) - np.mean(Pm)) < thresh):
			return True
		else:
			return False
	else:
		return False
