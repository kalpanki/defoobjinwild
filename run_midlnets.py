#!/usr/bin/python

from global_vars import *
import os
from generator import *
from trainer import *
from incrementor import *
from detector import *
import evaluator as eval
import scipy.io as sio
import shutil
import numpy as np
from utils import save_gt_data


test_mode = 'standard' # or 'standard'
train_mode = 'discard' # or increment
# conditions and difficulties considered
delOrder = ['easy', 'medium', 'hard']
imgConditions = ['fb', 'mb', 'po', 'de', 'eo', 'fv']
logFile = '>> output.log'
GT_FILES_PATH = ''

# generate test datset
if test_mode == 'standard':
	for cond in imgConditions:
		for delta in delOrder:
			mode = 'test'
			isInLoop = True
			k = 1
			print ("\nGenerating test data for condition: %s with difficulty: %s \n" % (cond, delta))
			totalMeshes = G_IMGS_PER_TEST_CONDITION * G_NUM_OBJECTS
			currGen = Generator(totalMeshes, mode, delta, isInLoop, k, cond, logFile)
			currGen.generate_data(False) # true will delete existing data
	GT_FILES_PATH = save_gt_data(cond)
	currGen.generate_proposals()

# ------------------------------------------------------------------------------
# start the incremental training
# ------------------------------------------------------------------------------
for cond in imgConditions:
	for currDel in delOrder:
		stop = False
		# to store the models at each iteration
		k = 1
		print ("Running training loop for Imaging Condition: %s with difficulty: %s in iteration: %s " % (cond, currDel, k))
		if test_mode == 'incremental':
			mode = 'test'
			isInLoop = True
			print ("\nGenerating test data for condition: %s with difficulty: %s \n" % (cond, delta))
			totalMeshes = G_IMGS_PER_TEST_CONDITION * G_NUM_OBJECTS
			currGen = Generator(totalMeshes, mode, delta, isInLoop, k, cond, logFile)
			currGen.generate_data(True) # delete previously existing data
		mAPScores = []
		while not stop:
			# deal with training data now
			mode = 'train'
			isInLoop = True
			totalMeshes = G_LOOP_MESHES_PER_OBJ * G_NUM_OBJECTS
			currGen = Generator(totalMeshes, mode, currDel, isInLoop, k, cond, logFile)
			currGen.generate_data(True) # if True, delete existing
			currGen.generate_proposals()
			currGen = 0
			# train with the training data
			latestModel = get_latest_model()
			print ("Going to TRAIN with model initialization %s " % latestModel)
			tr = Trainer(latestModel, logFile)
			latestModel = tr.train(G_LOOP_TRAINING_ITERATIONS)
			newFile = latestModel.strip('.caffemodel') + '_' + str(k) + '.caffemodel'
			print ("Latest model is: %s " % newFile)
			# shutil.copy(latestModel,newFile)
			# del latestModel
			# del tr
			# object detection - test using the test set
			testPrototxtFile = G_DETECTOR_PATH + '/models/DefoObject/' + 'test.prototxt'
			testProposalsFile = '/home/shrinivasan/work/defoobjinwild/dataset/test_proposals.mat'
			latestModel = get_latest_model()
			print ("Going to init Detector using the model: ", latestModel)
			currDetector = Detector(latestModel, testPrototxtFile, testProposalsFile, k, cond, currDel, logFile)
			predictResMatFile = currDetector.detect()
			# evaluate and keep appending the mAP scores to a list
			mAPScores.append(eval.evaluate_detections(GT_FILES_PATH, predictResMatFile))
			print mAPScores
			# stop if stopping criteria is met
			stop = stopping_criteria(mAPScores)
