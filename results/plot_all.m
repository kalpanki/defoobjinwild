clc; clear all; close all;

no_map = load('no_map_increasing_no_of_iterations');
easy_map = load('easy_map_increasing_no_of_iterations');
medium_map = load('medium_map_increasing_no_of_iterations');
hard_map = load('hard_map_increasing_no_of_iterations');

% increasing training data
%incr_map = load('../no_map.mat');
%incr_map = incr_map.map;

y_vals = [0 no_map.map easy_map.map medium_map.map hard_map.map];
% plot(0:26,y_vals); hold on;
% plot(0:26,y_vals,'ro');

y_vals_fixed = load('no_map_everything_fixed');
plot(0:20, [0 y_vals_fixed.map], 'g'); hold on;
plot(0:20, [0 y_vals_fixed.map], 'ro');
plot(0:7, [0 incr_map], 'm'); 
plot(0:7, [0 incr_map], 'ro'); 

ylim([0 1]);
xlabel('Iterations of Feedback loop');
ylabel('Mean IoU Scores');
hold off;
