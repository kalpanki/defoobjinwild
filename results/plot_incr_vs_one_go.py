#!/home/isit/anaconda2/bin/python

import plotly.graph_objs as go
import plotly.plotly as py
from plotly.offline import plot

trace_incr = go.Scatter(x = range(21), y = [0, 0.7, 0.7728, 0.7624, 0.7512, 0.7691, 0.7583, 0.7671, 0.7863, 0.7832,0.7739, 0.7428, 0.7624, 0.7582, 0.7743, 0.7803, 0.7882, 0.7627, 0.7882, 0.7672, 0.7729], 
					mode='lines+markers',name='Incremental Training')

trace_one_go = go.Scatter(x = range(21), y = [0.8120,0.8120,0.8120,0.8120,0.8120,0.8120,0.8120,0.8120,0.8120,0.8120,0.8120,0.8120,0.8120,0.8120,0.8120,0.8120,0.8120,0.8120,0.8120,0.8120,0.8120], 
					mode='lines',name='In one go')
data = [trace_incr,trace_one_go]

layout1 = go.Layout(autosize=False, margin=go.Margin(l = 80, r=80,b=80,t=80),
					yaxis=dict(range=[0,1],title='Mean IoU Score'), 
					xaxis = dict(title='Iterations of the loop'), 
					showlegend=True, legend = dict(x=0.9,y=1)) 


fig = go.Figure(data=data, layout = layout1)
plot(fig)
