#!/home/isit/anaconda2/bin/python

import plotly.graph_objs as go
import plotly.plotly as py
from plotly.offline import plot
import scipy.io as sio
import numpy as np

map_fb = sio.loadmat('fb_map.mat')
map_fb = map_fb['map']
map_fb = np.squeeze(map_fb)

map_mb = sio.loadmat('mb_map.mat')
map_mb = map_mb['map']
map_mb = np.squeeze(map_mb)

map_po = sio.loadmat('po_map.mat' )
map_po = map_po['map']
map_po = np.squeeze(map_po)

map_fv = sio.loadmat('fv_map.mat')
map_fv = map_fv['map']
map_fv = np.squeeze(map_fv)

map_de = sio.loadmat('de_map.mat' )
map_de = map_de['map']
map_de = np.squeeze(map_de)

map_eo = sio.loadmat('eo_map.mat' )
map_eo = map_eo['map']
map_eo = np.squeeze(map_eo)


trace_fb = go.Scatter(x = [1,2,3], y = map_fb, 
					mode='lines+markers',name='Focus Blur')
trace_fv = go.Scatter(x=[1,2,3],y=map_fv,
					mode='lines+markers',name='Field of View')
trace_eo = go.Scatter(x=[1,2,3],y=map_eo,
					mode='lines+markers',name='Occlusions')
trace_de = go.Scatter(x = [1,2,3],y=map_de,
					mode='lines+markers',name = 'Deformation')
trace_po = go.Scatter(x = [1,2,3],y= map_po,
					mode='lines+markers',name = 'Pose')
trace_mb = go.Scatter(x = [1,2,3], y = map_mb, 
					mode='lines+markers',name='Motion Blur')



data = [trace_fb,trace_mb,trace_eo,trace_po,trace_de,trace_fv]
layout = go.Layout(autosize=False, margin=go.Margin(l = 80, r=80,b=80,t=80),
					title = 'Progressive Training',
					xaxis=dict(ticktext=["Easy","Medium","Hard"],tickvals=[1,2,3], title='Difficulty'),
					yaxis=dict(title = 'Mean IoU Score'))
fig = go.Figure(data=data,layout=layout)
plot(fig)
