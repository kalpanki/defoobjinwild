#!/home/isit/anaconda2/bin/python

import plotly.graph_objs as go
import plotly.plotly as py
from plotly.offline import plot

trace_fb = go.Scatter(x = [1,2,3], y = [0.8951 ,   0.8731 ,   0.8701], 
					mode='lines+markers',name='Focus Blur')
trace_fv = go.Scatter(x=[1,2,3],y=[0.7283 ,   0.7435 ,   0.7541],
					mode='lines+markers',name='Field of View')
trace_eo = go.Scatter(x=[1,2,3],y=[0.7130  ,  0.7225 ,   0.7148],
					mode='lines+markers',name='Occlusions')
trace_de = go.Scatter(x = [1,2,3],y=[0.7232  ,  0.7074,    0.6856],
					mode='lines+markers',name = 'Deformation')
trace_po = go.Scatter(x = [1,2,3],y=[0.7756  ,  0.7259,    0.7100],
					mode='lines+markers',name = 'Pose')
trace_mb = go.Scatter(x = [1,2,3], y = [0.8677  ,  0.8694  ,  0.8703], 
					mode='lines+markers',name='Motion Blur')



data = [trace_fb,trace_mb,trace_eo,trace_po,trace_de,trace_fv]
layout = go.Layout(xaxis=dict(ticktext=["Easy","Medium","Hard"],tickvals=[1,2,3]))
fig = go.Figure(data=data,layout=layout)
plot(fig)
