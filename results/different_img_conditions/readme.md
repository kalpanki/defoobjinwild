These results are obtained by running:
```run_scenario_three.py```

Imaging conditions considered:
Focus Blur, Motion Blur, Pose Change, Deformation, Self Occlution, 
External Occlution, Field of View/Scale change.

The results obtainned are for less data:

* initialization with: Simple model obtained by initializing with ImageNet and training for 30,000 iterations.
* In the loop, Different parameters are:
	* 2000 iterations for training
	* 10 new training images per iteration of loop
	* 10 new test images per iteration of loop
	
* Parameters are defined as below:

```
G_NUM_OBJECTS = 1
G_MESHES_PER_OBJ = 200
G_LOOP_MESHES_PER_OBJ = 10
# number of iterations to train the Fast R-CNN detector
G_LOOP_TRAINING_ITERATIONS = 2000
# Number of images to be generated per object (class) for each test cycle
G_TEST_M = 200
# passed to render_mesh.py for test mode
G_IMGS_PER_TEST_CONDITION = 10
```