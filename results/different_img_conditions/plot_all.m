clc; close all; clear all;

load('fb_map');
fb_map = map(1:3);
eo_map = map(4:6);
load('de_map');
de_map = map;
load('bb_map');
mb_map = map;
load('fv_map');
fv_map = map;
load('po_map');
po_map = map;

Y = [fb_map; mb_map; po_map; de_map; eo_map; fv_map];
gca = figure;
figure; plot(Y','LineWidth',2); hold on; plot(Y','r*');
legend('Focus Blur', 'Motion Blur', 'Pose', 'Deformation', 'Occlusions', 'Field of View');
