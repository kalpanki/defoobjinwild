#!/home/isit/anaconda2/bin/python

from global_vars import *
import os
import subprocess


class Trainer:
	'''
	Trainer doesn't know what data is available. It just works on the training data
	'''

	def __init__(self, init_model, log_file):
		self.log_file = log_file
		self.init_model = init_model

	def train(self, num_iter):
		'''
		Start the trainig of the Fast R-CNN Detector
		and returns the trained model when done.
		'''
		
		print ("In Trainer: Starting to train")
		os.chdir(G_DETECTOR_PATH)
		if self.init_model == '':
			# train without initialization - should be lots of iterations
			os.system('tools/train_net.py \
						--gpu 0 \
						--solver models/DefoObject/solver.prototxt \
						--imdb defo_object_train \
						--iters %s %s' % (num_iter, self.log_file))
		else:
			# train by initialization with weights provided
			os.system('tools/train_net.py \
						--gpu 0 \
						--solver models/DefoObject/solver.prototxt \
						--imdb defo_object_train \
						--weights %s \
						--iters %s %s' % (self.init_model, num_iter, self.log_file))

		# if trained model exists, return it
		model_fname = G_DETECTOR_PATH + '/output/default/train/defo_obj_fast_rcnn_iter_' + str(num_iter) + '.caffemodel'
		if os.path.isfile(model_fname):
			return model_fname
		else:
			print ("Training maynot be successful. Trained model is not available.")


	def get_latest_model(self):
		'''
		Gets the latest saved file from the output/default/*.caffemodel files
		'''
		os.chdir(G_DETECTOR_PATH + '/output/default/train/')
		cmd = 'ls -t1 | head -n 1'
		proc = subprocess.Popen(cmd, stdout=subprocess.PIPE,shell=True)
		(op,err) = proc.communicate()
		op = op.rstrip('\n')

		op = G_DETECTOR_PATH + '/output/default/train/' + op
		return op



if __name__ == '__main__':
	if G_IS_LOGGING:
		log_file = ' >> output.log'
	else:
		log_file = ' > /dev/null 2>&1'


	t = Trainer(log_file)
	t.train(1000)