#!/home/isit/anaconda2/bin/python

from global_vars import *
import os
from generator import *
from trainer import *
import caffe, sys, cv2
import argparse
import scipy.io as sio
import matplotlib.pyplot as plt
import xml.etree.ElementTree as ET
import numpy as np


class Detector:

	def __init__(self, caffemodel, prototxt, proposals_file, test_iter, cond, difficulty, log_file):
		self.prototxt = prototxt
		#this.caffemodel = os.path.join(G_DETECTOR_PATH, 'output', 'default', 'train', NETS[args.demo_net][1])
		self.caffemodel = caffemodel
		self.proposals_file = proposals_file
		self.test_iter = test_iter
		self.cond = cond
		self.difficulty = difficulty


	def detect(self):
		# check if the model file and prototxt exist
		if not os.path.exists(self.caffemodel):
			print "The trained model file does not exist"
			return
		if not os.path.exists(self.prototxt):
			print "The prototxt file does not exist"
			return
		
		# do actual detection
		os.chdir(G_DETECTOR_PATH)
		os.system('tools/detect_defo_object.py \
						--path %s \
						--prop %s \
						--model %s \
						--prototxt %s \
						--num_objects %s \
						--cpu \
						--imgs_per_class %s \
						--test_iter %s \
						--cond %s \
						--difficulty %s' % (G_TEST_IMAGES_SET, self.proposals_file ,self.caffemodel, self.prototxt, G_NUM_OBJECTS, G_TEST_M, self.test_iter, self.cond, self.difficulty))

		return '/home/isit/workspace/DefoObjInWild/dataset/pr_bboxes.mat'


	def get_gt_data(self):
		test_set_file = '/home/isit/workspace/DefoObjInWild/dataset/ImageSets/test.txt'
		with open(test_set_file) as f:
			lines = f.readlines()
		# to store ground truth
		gt_bboxes = np.zeros((len(lines),5))

		for l in range(len(lines)):
			line = lines[l]
			line = line.strip('\n')
			print ("line is: ", line)
			xml_file = '/home/isit/workspace/DefoObjInWild/dataset/' + 'TestAnnotations/' + line + '.xml'
			tree = ET.parse(xml_file)
			root = tree.getroot()
			# get class index
			class_idx = (root[1][0].text).strip('object_')
			gt_bboxes[l,-1] = int(class_idx)
			# get the box locations
			for i in range(4):
				gt_bboxes[l,i] = int(root[1][1][i].text)
				print ("in for loop: ", gt_bboxes[l,i])

		print ("GT BOXES ARE: ", gt_bboxes)

		gt_dest = '/home/isit/workspace/DefoObjInWild/dataset' + '/gt_bboxes.mat'
		sio.savemat(gt_dest, {'gt_bboxes' : gt_bboxes})
		return gt_dest



if __name__ == '__main__':
	args = parse_args()

	if args.cpu_mode:
		caffe.set_mode_cpu()
	else:
		caffe.set_mode_gpu()
		caffe.set_device(args.gpu_id)
	net = caffe.Net(prototxt, caffemodel, caffe.TEST)

	print '\n\nLoaded network {:s}'.format(caffemodel)
	print 'Demo for data/demo_defo_object/object1_img1150.jpg'
	demo(net, 'object1', ('object_1',))

	#print 'Demo for data/demo_defo_object/object2_img1150.jpg'
	#demo(net, 'object2_img1150', ('object_2'))

	plt.show()
