'''
Runs modified fast R-CNN algorithm for detection.
This will run the traditional way but by increasing the test training data every time:
1. Generate test data for 'simple' or 'no hard' imaging conditions
2. Generate train data for 'simple' or 'no hard' imaging conditions

Imaging conditions considered:
Minimal deformations - toy examples
'''

from global_vars import *
import os
from generator import *
from trainer import *
from incrementor import *
from detector import *
from evaluator import *
import scipy.io as sio
import shutil

# some initializations
completed = False
logFile = ' > output.log'
imgConditions = ['fb', 'mb', 'po', 'de', 'eo', 'fv']
delOrder = ['easy', 'medium', 'hard']
# imgConditions = ['no']
# delOrder = ['no']
# A common incrementor object to use in our loop
commonInc = Incrementor(delOrder, logFile)
commonInc.curr_del = 'no'


# useful function
def get_latest_model():
	os.chdir(G_DETECTOR_PATH + '/output/default/train/')
	cmd = 'ls -t1 | head -n 1'
	proc = subprocess.Popen(cmd, stdout=subprocess.PIPE,shell=True)
	(op,err) = proc.communicate()
	op = op.rstrip('\n')
	op = G_DETECTOR_PATH + '/output/default/train/' + op
	return op


for k in range(9):
	# generate training data for all cond+difficulty
	mode = 'train'
	isInLoop = True
	imgConditions = ['no']
	delOrder = ['no']
	for cond in imgConditions:
		for currDel in delOrder:
			print ("\nGenerating training data for condition: %s with difficulty: %s \n" % (cond, currDel))
			commonInc.curr_del = currDel
			# generate training data
			totalMeshes = G_LOOP_MESHES_PER_OBJ
			currGen = Generator(totalMeshes, mode, commonInc.curr_del, isInLoop, k, cond, logFile)
			# False - do not delete existing data
			currGen.generate_data(False)
			currGen.generate_proposals()
			currGen = 0	
	# train using the training data
	latestModel = get_latest_model()
	print ("Going to TRAIN with model initialization %s " % latestModel)
	tr = Trainer(latestModel, logFile)
	latestModel = tr.train(G_LOOP_TRAINING_ITERATIONS)
	newFile = latestModel.strip('.caffemodel') + '_' + str(k) + '.caffemodel'
	print ("Latest model is: %s " % newFile)
	shutil.copy(latestModel,newFile)
	latestModel = 0
	tr = 0
	# test using the data
	testPrototxtFile = G_DETECTOR_PATH + '/models/DefoObject/' + 'test.prototxt'
	testProposalsFile = '/home/isit/workspace/DefoObjInWild/dataset/test_proposals.mat'
	latestModel = get_latest_model()
	print ("Going to init Detector using the model: ", latestModel)
	currDetector = Detector(latestModel, testPrototxtFile, testProposalsFile, k+1, logFile)
	predictResMatFile = currDetector.detect()
	#gtMatFile = currDetector.get_gt_data()

	# evaluate and store the pr_bboxes file
	# 'simple', 'no', 'no', '1' - scenario, condition, difficulty, iter_loop
	#currEval = Evaluator(gtMatFile, predictResMatFile, 'simple', 'no', 'no', '1', logFile)
	# status = currEval.evaluate_detections()