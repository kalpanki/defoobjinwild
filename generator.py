#!/home/isit/anaconda2/bin/python

from global_vars import *
import os
from shutil import copyfile
import random
import dlib
from skimage import io 
import cv2

class Generator:

	def __init__(self, total_meshes, mode, curr_del, in_loop, iter_k, cond, log_file):
		self.total_meshes = total_meshes
		self.mode = mode
		self.curr_del = curr_del
		self.cond = cond
		self.log_file = log_file
		self.in_loop = in_loop
		self.iter_k = iter_k
		self.focus_del = {'no'     : [1],
						  'easy'   : range(3,7,2),
			 			  'medium' : range(7,13,2),
			 			  'hard'   : range(11,21,2)}

		if in_loop:
			self.meshes_per_obj = G_LOOP_MESHES_PER_OBJ
		else:
			self.meshes_per_obj = G_MESHES_PER_OBJ

	def get_rand_param(self):
		'''
		del_val : difficulty level. Eg, 'easy'
		'''
		return random.choice(self.focus_del[self.curr_del])

	def generate_mesh(	self ):
		'''
		Generate some meshes or deformations in Matlab to be used 
		for rendering in the following steps.
		'''
		status = self.clean_temp_directories()
		if not status:
			print "Not able to clean temp directories"

		# default mesh params
		num_rulings = 1; num_regions = 3
		if self.cond == 'de':
			if self.curr_del == 'easy':
				num_rulings = 3; num_regions = 3
			if self.curr_del == 'medium':
				num_rulings = 5; num_regions = 4
			if self.curr_del == 'hard':
				num_rulings = 8; num_regions = 5

		print "Generating deformations in Matlab"
		matlab_cmd = "addpath(genpath('%s')); generateMeshes('%s', '%s', '%s');" % \
					((G_PROJECT_ROOT + '/paper_model'), self.total_meshes, num_rulings, num_regions)
		print ("Going to run Matlab command: ", matlab_cmd)
		try:
			os.system('%s -nodisplay -r "try %s ; catch; end; quit;" %s' % \
				(G_MATLAB_ROOT, matlab_cmd, self.log_file))
		except:
			print ("Problem executing matlab command: %s" % matlab_cmd)
			return False

		return True


	def render_mesh( self ):
		'''
		This creates and renders the meshes and stores them in the  
		temp_rendered directory 
		'''
		print "Rendering meshes in blender"
		try:
			blender_project = G_PROJECT_ROOT + '/utils/project_1.blend'
			render_code = G_PROJECT_ROOT + '/utils/render_mesh.py'
			if self.mode == 'train':
				blend_cmd = '%s %s --background --python %s -- %s %s %s %s' % \
							(G_BLENDER_ROOT, blender_project, render_code, G_NUM_OBJECTS, self.meshes_per_obj, self.cond, self.curr_del)
			else: # test mode
				blend_cmd = '%s %s --background --python %s -- %s %s %s %s' % \
							(G_BLENDER_ROOT, blender_project, render_code, G_NUM_OBJECTS, G_IMGS_PER_TEST_CONDITION, self.cond, self.curr_del)
			print blend_cmd
			os.system('%s %s' % (blend_cmd, self.log_file))
		except:
			print("Rendering failed ! Tried command: %s" % blend_cmd)
			return False
		return True


	def add_background( self ):
		'''
		This adds random backgrounds to the generated and rendered meshes.
		Images get stored in dataset/Images directory.
		Annotations get stored in dataset/Annotations directory.
		'''

		print "Adding background in Matlab"
		if self.mode == 'train':
			matlab_cmd = "addpath(genpath('%s')); addBackground('%s', '%s', '%s', '%s', '%s', '%s');" % \
				(G_PROJECT_ROOT, G_NUM_OBJECTS, self.meshes_per_obj, self.mode, self.curr_del, self.iter_k, self.cond)
		else: # test mode
			matlab_cmd = "addpath(genpath('%s')); addBackground('%s', '%s', '%s', '%s', '%s', '%s');" % \
				(G_PROJECT_ROOT, G_NUM_OBJECTS, G_IMGS_PER_TEST_CONDITION, self.mode, self.curr_del, self.iter_k, self.cond)
		print ("Going to run Matlab command: %s %s %s " % (G_MATLAB_ROOT,matlab_cmd, self.log_file))

		try:
			os.system('%s -nodisplay -r "try %s ; catch; end; quit;" %s' % \
					(G_MATLAB_ROOT, matlab_cmd, self.log_file))
		except:
			print "Problem with adding background"
			return False
		return True


	def create_train_imagesets( self ):
		'''
		Create and store the train.txt files in the 
		dataset/ImageSets folder
		'''
		images_folder = G_PROJECT_ROOT + '/dataset/Images'
		dest = G_PROJECT_ROOT + '/dataset/ImageSets/train.txt'
		if (os.path.isfile(dest)):
			os.system("rm %s" % dest)
		os.system("ls -1 %s | sed -e 's/\..*$//' > %s " % (images_folder, dest))
		return True


	def create_test_imagesets( self ):	
		'''
		Create and store the test.txt files in the 
		dataset/ImageSets folder
		'''
		images_folder = G_PROJECT_ROOT + '/dataset/TestImages'
		dest = G_PROJECT_ROOT + '/dataset/ImageSets/test.txt'
		if (os.path.isfile(dest)):
			os.system("rm %s" % dest)
		os.system("ls -1 %s | sed -e 's/\..*$//' > %s " % (images_folder, dest))
		return True


	def generate_proposals( self ):
		'''
		Generates proposals and saves in /dataset/test_proposals.mat or 
		/dataset/train_proposals.mat based on training or testing mode
		'''
		# use selective search - slow
		#os.chdir(G_DETECTOR_PATH + '/selective_search')
		#matlab_cmd = "addpath(genpath('%s')); selective_search('%s');" % ((G_DETECTOR_PATH), self.mode)

		# use edge boxes - faster
		os.chdir(G_DETECTOR_PATH)
		matlab_cmd = "addpath(genpath('%s')); gen_edgeboxes('%s');" % ((G_DETECTOR_PATH), self.mode)

		print ("Going to run Matlab command: ", matlab_cmd)
		try:
			os.system('%s -nodisplay -r "try %s ; catch; end; quit;" %s' % \
				(G_MATLAB_ROOT, matlab_cmd, self.log_file))
		except:
			print ("Problem executing matlab command: %s" % matlab_cmd)
			return False

		os.chdir(G_PROJECT_ROOT)
		fdest = G_PROJECT_ROOT + ''
		if self.mode == 'test':
			#copyfile(G_DETECTOR_PATH + '/selective_search/test_proposals.mat', G_PROJECT_ROOT + '/dataset/test_proposals.mat')
			copyfile(G_DETECTOR_PATH + '/edge_boxes/test_proposals.mat', G_PROJECT_ROOT + '/dataset/test_proposals.mat')
		else:
			#copyfile(G_DETECTOR_PATH + '/selective_search/train_proposals.mat', G_PROJECT_ROOT + '/dataset/train_proposals.mat')
			copyfile(G_DETECTOR_PATH + '/edge_boxes/train_proposals.mat', G_PROJECT_ROOT + '/dataset/train_proposals.mat')
		return True


	def generate_py_proposals( self ):
		''' 
		Generate proposals using the fast python library instead of Matlab one
		'''
		# iterate files and do detection
		if self.mode == 'train':
			train_set_file = G_TEST_IMAGES_SET + '/train.txt'
		
		with open(train_set_file) as f:
			lines = f.readlines()

		# iterate each line (image) in file 
		for img_idx in range(len(lines)):
			curr_line = lines[img_idx]
			curr_line = curr_line.strip('\n')
			im_file = os.path.join(G_TEST_IMAGES_SET + '/' + '../Images/' + curr_line + '.jpg')
			im = cv2.imread(im_file)
			props = []
			rects = []

			dlib.find_candidate_object_locations(im, rects, min_size=500)
			for k,d in enumerate(rects):
				props.append([d.left(),d.top(),d.right(),d.bottom()])
			# proposals for the image as np array				
			props = np.array(props)

			# save proposals in the needed format as mat file



	def clean_train_directories(self):
		'''
		Clean the directories Images, ImageSet and Annotations.
		Also clean train.mat, test.mat. 
		This must be used before generating fresh training data.
		'''

		os.chdir(G_PROJECT_ROOT + '/dataset/Annotations')
		filelist = [ f for f in os.listdir(".") if f.endswith(".xml")]
		if len(filelist) > 0:
			for f in filelist:
				os.remove(f)
			print ("Deleted files in Annotations folder")

		os.chdir(G_PROJECT_ROOT + '/dataset/Images')
		filelist = [ f for f in os.listdir(".") if f.endswith(".jpg")]
		if len(filelist) > 0:
			for f in filelist:
				os.remove(f)
			print ("Deleted files in Images folder")

		os.chdir(G_PROJECT_ROOT + '/dataset/ImageSets')
		filelist = [ f for f in os.listdir(".") if f.endswith("train.txt")]
		if len(filelist) > 0:
			for f in filelist:
				os.remove(f)
			print ("Deleted files in ImageSets folder")

		os.chdir(G_PROJECT_ROOT + '/dataset')
		filelist = [ f for f in os.listdir(".") if f.endswith("train_proposals.mat")]
		if len(filelist) > 0:
			for f in filelist:
				os.remove(f)
			print ("Deleted test and train mat files")

		return True


	def clean_pkl_files(self):
		''' 
		Delete the cached files in fast-rcnn/data/cache folder
		'''
		os.chdir(G_DETECTOR_PATH + '/data/cache')
		filelist = [ f for f in os.listdir(".") if f.endswith(".pkl")]
		if len(filelist) > 0:
			for f in filelist:
				os.remove(f)
		print ("Deleted cached pkl files")


	def clean_test_directories(self):
		os.chdir(G_PROJECT_ROOT + '/dataset/Annotations')
		filelist = [ f for f in os.listdir(".") if f.endswith(".jpg")]
		if len(filelist) > 0:
			for f in filelist:
				os.remove(f)
			print ("Deleted files in TestImages folder")

		os.chdir(G_PROJECT_ROOT + '/dataset/TestAnnotations')
		filelist = [ f for f in os.listdir(".") if f.endswith(".xml")]
		if len(filelist) > 0:
			for f in filelist:
				os.remove(f)
			print ("Deleted files in Annotations folder")

		os.chdir(G_PROJECT_ROOT + '/dataset/TestImages')
		filelist = [ f for f in os.listdir(".") if f.endswith(".jpg")]
		if len(filelist) > 0:
			for f in filelist:
				os.remove(f)
			print ("Deleted files in Images folder")

		os.chdir(G_PROJECT_ROOT + '/dataset/ImageSets')
		filelist = [ f for f in os.listdir(".") if f.endswith("test.txt")]
		if len(filelist) > 0:
			for f in filelist:
				os.remove(f)
			print ("Deleted files in ImageSets folder")

		os.chdir(G_PROJECT_ROOT + '/dataset')
		filelist = [ f for f in os.listdir(".") if f.endswith("test_proposals.mat")]
		if len(filelist) > 0:
			for f in filelist:
				os.remove(f)
			print ("Deleted test and train mat files")

		return True


	def clean_temp_directories(self):
		print "Cleaning temp directories: temp_rendered and temp_meshes"
		os.chdir(G_PROJECT_ROOT + '/dataset/temp_meshes')
		filelist = [ f for f in os.listdir(".") if f.endswith(".obj")]
		if len(filelist) > 0:
			for f in filelist:
				os.remove(f)
			print ("Deleted files in temp_meshes folder")

		os.chdir(G_PROJECT_ROOT + '/dataset/temp_rendered')
		filelist = [ f for f in os.listdir(".") if f.endswith(".png")]
		if len(filelist) > 0:
			for f in filelist:
				os.remove(f)
			print ("Deleted files in temp_rendered folder")

		return True


	def generate_data(self, should_clean = True):
		'''
		Do all the steps needed to generate the data.
		Be it training or testing data.
		'''
		# clean before creating data
		if self.mode == 'train':
			self.clean_pkl_files()
			if should_clean:
				self.clean_train_directories()
			
		if self.mode == 'test' and should_clean:
			self.clean_test_directories()
		self.clean_temp_directories()

		print ("Generating data for mode: %s with imaging condition: %s" % (self.mode, self.curr_del))
		status = True
		# generate data now
		status = self.generate_mesh()
		print ("Generating mesh successful: ", status)
		if status:
			status = self.render_mesh()
			print ("Rendering mesh successful: ", status)
		if status:
			status = self.add_background()
			print ("Adding background successful: ", status)
		if status:
			if (self.mode == 'train'):
				status = self.create_train_imagesets()
				print ("Creating ImageSets successful: ", status)
			else:
				status = self.create_test_imagesets()
				print ("Creating test ImageSets successful: ", status)



if __name__ == '__main__':
	# take care of logging first
	if G_IS_LOGGING:
		log_file = ' >> output.log'
	else:
		log_file = ' > /dev/null 2>&1'

	# out workflow to generate
	total_meshes = G_NUM_OBJECTS * G_MESHES_PER_OBJ
	print ("Starting workflow")
	#generate_mesh(total_meshes, log_file)
	#render_mesh( log_file )
	#add_background(log_file)

	print ("Finished with workflow to generate data!")
