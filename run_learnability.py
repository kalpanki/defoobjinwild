#!/usr/bin/python

from global_vars import *
import os
from generator import *
from trainer import *
from incrementor import *
from detector import *
import evaluator as eval
import scipy.io as sio
import shutil
import numpy as np
from utils import save_gt_data,get_latest_model,stop_criteria, clean_for_learnability

# conditions and difficulties considered
delOrder = ['easy', 'medium', 'hard']
imgConditions = ['fb', 'mb', 'po', 'de', 'eo', 'fv']
logFile = '>> output.log'
GT_FILES_PATH = '/home/shrinivasan/work/defoobjinwild/dataset/gt_bboxes.mat'

for cond in imgConditions:
    # clean all test data
    clean_for_learnability()
    # generate test set for all difficulty levels
    for currDel in delOrder:
        mode = 'test'
        isInLoop = True
        k = 1
        print ("\nGenerating test data for condition: %s with difficulty: %s \n" % (cond, currDel))
        totalMeshes = G_IMGS_PER_TEST_CONDITION * G_NUM_OBJECTS
        currGen = Generator(totalMeshes, mode, currDel, isInLoop, k, cond, logFile)
        currGen.generate_data(False) # true will delete existing data
    GT_FILES_PATH = save_gt_data()
    currGen.generate_proposals()
    # train for each difficulty
    for currDel in delOrder:
        k = 1
        stop = False
        mAPScores = []
        while k < 12:
            mode = 'train'
            isInLoop = True
            totalMeshes = G_LOOP_MESHES_PER_OBJ * G_NUM_OBJECTS
            currGen = Generator(totalMeshes, mode, currDel, isInLoop, k, cond, logFile)
            currGen.generate_data(True)
            currGen.generate_proposals()
            latestModel = get_latest_model()
            tr = Trainer(latestModel, logFile)
            latestModel = tr.train(G_LOOP_TRAINING_ITERATIONS)
            newFile = latestModel.strip('.caffemodel') + '_' + str(k) + '.caffemodel'
            shutil.copy(latestModel,newFile)
            testPrototxtFile = G_DETECTOR_PATH + '/models/DefoObject/' + 'test.prototxt'
            testProposalsFile = '/home/shrinivasan/work/defoobjinwild/dataset/test_proposals.mat'
            latestModel = get_latest_model()
            currDetector = Detector(latestModel, testPrototxtFile, testProposalsFile, k, cond, currDel, logFile)
            if currDetector.detect():
                predictResMatFile = G_PROJECT_ROOT + '/dataset/predictions/' +  'pr_bboxes_' + cond + '_' + currDel + '_' + str(k) + '.mat'
            mAPScores.append(eval.evaluate_detections(GT_FILES_PATH, predictResMatFile))
            print mAPScores
            k += 1
        map_file = G_PROJECT_ROOT + '/dataset/predictions/map_scores_' + cond + '_' + currDel + '.mat'
        sio.savemat(map_file, {'map': mAPScores})
