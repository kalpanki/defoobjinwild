% given the directory where the *.mat files of the results are located,
% plots the results 
function [] = plot_results(res_dir)
    clc; clear all; close all;
    res_dir = '/home/shrinivasan/work/defoobjinwild/dataset/results_incr_test_increment_train/';
    save_dest = '/home/shrinivasan/work/latex/journal_paper/figures/results_incr_test_increment_train/';
    % should we save plots?
    do_save = 1; 
    conds = {'fb','mb', 'po', 'de', 'eo', 'fv'};
    diffs = {'easy', 'medium', 'hard'};

    for c = 1:numel(conds)
        fig_hdl = figure; 
        hold on;
        for d = 1:numel(diffs)
            map_fname = strcat(res_dir, 'map_scores_', conds(c), '_', diffs(d), '.mat')
            load(char(map_fname));
            h_line = plot(map,'LineWidth',1.5); 
            h_dots(d) = plot(map);
            clr = get_colour(diffs(d));
            set(h_line,'Color', clr);
            set(h_dots(d),'Marker', 'o', 'MarkerSize', 6, 'MarkerFaceColor' , clr);
            ylim([0 1]);
        end
        do_labels(conds(c));
        % add grid
        set(gca,'YGrid', 'on');
        set(gca,'XGrid', 'on');
        legend(h_dots, 'Easy', 'Medium','Hard','location','SouthEast');
        hold off;
        
        if do_save
            saveas(fig_hdl, strcat(save_dest,char(conds(c))), 'epsc');
        end
    end
end


function c = get_colour(diff)
    if (strcmp(diff,'easy'))
        c = [1 0.75 0.75];
    end
    if (strcmp(diff,'medium'))
        c = [0.75 1 0.75];
    end
    if (strcmp(diff,'hard'))
        c = [0.75 0.75 1];
    end
end


function [] = do_labels(cond)
    if strcmp(cond,'fb')
        ttl = 'Focus Blur';
    end
    if strcmp(cond, 'mb')
        ttl = 'Motion Blur';
    end
    if strcmp(cond, 'eo')
        ttl = 'External Occlusion';
    end
    if strcmp(cond, 'po')
        ttl = 'Pose';
    end
    if strcmp(cond, 'de')
        ttl = 'Deformation';
    end
    if strcmp(cond, 'fv')
        ttl = 'Scale';
    end
    hTitle  = title(ttl);
    hXLabel = xlabel('Iterations of Loop, k');
    hYLabel = ylabel('mAP Score');
    set([hTitle, hXLabel, hYLabel], 'FontName'   , 'AvantGarde');
    set([hXLabel, hYLabel], 'FontSize', 11);
    set( hTitle, 'FontSize', 13); %'FontWeight' , 'bold');
end
