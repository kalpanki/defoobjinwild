function [ precision, recall ] = evaluate_detections( gt, pr )
%EVALUATE_DETECTIONS Summary of this function goes here
%   Detailed explanation goes here
    load(gt);
    load(pr);
    % compute everything by iterating all predictions
    TP = 0; FP = 0; FN = 0;
    for pb = 1:size(pr_bboxes,1)
        pr_xmin = pr_bboxes(pb,1); pr_ymin = pr_bboxes(pb,2); 
        pr_xmax = pr_bboxes(pb,3); pr_ymax = pr_bboxes(pb,4);
        pr_width = pr_xmax - pr_xmin;
        pr_height = pr_ymax - pr_ymin;
        pr_fmtd = [pr_xmin, pr_ymin, pr_width, pr_height];
        im_no = pr_bboxes(pb,7) + 1;
        % get our needed values - for ground truth
        gt_xmin = gt_bboxes(im_no,1); gt_ymin = gt_bboxes(im_no,2);
        gt_xmax = gt_bboxes(im_no,3); gt_ymax = gt_bboxes(im_no,4);
        gt_width = gt_xmax - gt_xmin;
        gt_height = gt_ymax - gt_ymin;
        gt_fmtd = [gt_xmin, gt_ymin, gt_width, gt_height];
        
        intersectionArea = rectint(gt_fmtd,pr_fmtd);
        unionCoords = [min(gt_xmin,pr_xmin), ...
                       min(gt_ymin,pr_ymin), ...
                       max(gt_xmin + gt_width - 1, pr_xmin + pr_width - 1), ...
                       max(gt_ymin + gt_height - 1,pr_ymin + pr_height - 1)];
        unionArea = (unionCoords(3)-unionCoords(1)+1)*(unionCoords(4)-unionCoords(2)+1);
        %This should be greater than 0.5 to consider it as a valid detection.
        iou = intersectionArea / unionArea;
        if (iou > 0.5)
            TP = TP + 1;
        else
            if pr_bboxes(pb,6) == 0
                FP = FP + 1;
            end
        end
    end
        FN = sum(pr_bboxes(:,6));
        % precision = TP / (TP+FP);
         precision = TP / size(pr_bboxes,1);
        recall = TP / (TP+FN);

end

