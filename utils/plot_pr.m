function [ ] = plot_pr(gt, cond, diff)
% gt - path to the ground truth .mat file
%      Eg. '/home/isit/workspace/DefoObjInWild/results_final/incr_conditions/test_set_per_cond_caffe_init/predictions/gt_bboxes_fv_all.mat';
% cond - condition we need to run the plot for
% diff - difficulty under the condition we are considering
    
    map = [];
    figure; hold on;
    pr_path = '/home/shrinivasan/work/defoobjinwild/dataset/predictions/';

    % iterate over all files under predictions for the given condition 
    % and difficulty
    % for i = 1:10
    for i = 1:1
        % pr = strcat('/home/isit/workspace/DefoObjInWild/dataset/pr_bboxes_fv_hard_', int2str(i), '.mat');
        %pr = strcat('/home/isit/workspace/DefoObjInWild/results_final/incr_conditions/test_set_per_condition_diff/predictions/pr_bboxes_de_easy_', ...
        %    int2str(i),'.mat');
        %pr = strcat('/home/isit/workspace/DefoObjInWild/results_final/incr_conditions/test_set_per_cond_caffe_init/predictions/pr_bboxes_fb_easy_', ...
        %    int2str(i),'.mat');
        pr = strcat(pr_path, 'pr_bboxes_', cond, '_', diff, '_', int2str(i), '.mat');
        [precision, recall, info] = get_pr(gt,pr);
        % keep concatenating results in map 
        map = [map info.ap];
    end

    plot(map, 'lineWidth',2); plot(map,'bo','MarkerSize',10); ylim([0 1]);
    save_path = '/home/shrinivasan/work/defoobjinwild/results/incr_conditions/';
    save_fname = strcat(save_path, 'map','_', cond, '_', diff, '.mat');
    save(save_fname,'map');

end
