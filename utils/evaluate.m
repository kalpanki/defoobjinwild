function [] = evaluate(gt, pr) 
% computer area of intersection of rectangles gt and pr
% gt, pr - names of the mat files for ground truth and predictions
%
% gt = [xmin, ymin, xmax, ymax]
% pr = [xmin, ymin, xmax, ymax]

    load(gt);
    load(pr);
    
    IoU = zeros(1,length(gt_bboxes));
    for i = 1:length(gt_bboxes)
        % if the predicted class is different, just move on
        if (gt_bboxes(i,5) ~= pr_bboxes(i,5))
           continue;
        end
        
        % get our needed values
        gt_xmin = gt_bboxes(i,1); gt_ymin = gt_bboxes(i,2);
        gt_xmax = gt_bboxes(i,3); gt_ymax = gt_bboxes(i,4);
        gt_width = gt_xmax - gt_xmin;
        gt_height = gt_ymax - gt_ymin;
        gt_fmtd = [gt_xmin, gt_ymin, gt_width, gt_height];

        pr_xmin = pr_bboxes(i,1); pr_ymin = pr_bboxes(i,2); 
        pr_xmax = pr_bboxes(i,3); pr_ymax = pr_bboxes(i,4);
        pr_width = pr_xmax - pr_xmin;
        pr_height = pr_ymax - pr_ymin;
        pr_fmtd = [pr_xmin, pr_ymin, pr_width, pr_height];

        intersectionArea = rectint(gt_fmtd,pr_fmtd);
        unionCoords = [min(gt_xmin,pr_xmin), ...
                       min(gt_ymin,pr_ymin), ...
                       max(gt_xmin + gt_width - 1, pr_xmin + pr_width - 1), ...
                       max(gt_ymin + gt_height - 1,pr_ymin + pr_height - 1)];
        unionArea = (unionCoords(3)-unionCoords(1)+1)*(unionCoords(4)-unionCoords(2)+1);

        %This should be greater than 0.5 to consider it as a valid detection.
        IoU(i) = intersectionArea / unionArea; 
    end
    save('/home/isit/workspace/DefoObjInWild/dataset/iou_scores.mat', 'IoU');
    
end