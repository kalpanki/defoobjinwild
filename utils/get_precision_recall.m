function [recall,precision,info] = get_precision_recall(gt, pr,scenario,cond,diff,loop_iter)

    load(gt);
    load(pr);
    % load('/home/isit/workspace/DefoObjInWild/dataset/pr_bboxes.mat');
    % vlfeat toolbox
    addpath(genpath('/home/shrinivasan/work/vlfeat/toolbox'));
    vl_setup;
    
    % generate labels and scores
    for pb = 1:size(pr_bboxes,1)
        % check if the detection is positive or negative based on IoU score
        % get our needed values - for predictions
        pr_xmin = pr_bboxes(pb,1); pr_ymin = pr_bboxes(pb,2); 
        pr_xmax = pr_bboxes(pb,3); pr_ymax = pr_bboxes(pb,4);
        pr_width = pr_xmax - pr_xmin;
        pr_height = pr_ymax - pr_ymin;
        pr_fmtd = [pr_xmin, pr_ymin, pr_width, pr_height];
        im_no = pr_bboxes(pb,7) + 1;
        % get our needed values - for ground truth
        gt_xmin = gt_bboxes(im_no,1); gt_ymin = gt_bboxes(im_no,2);
        gt_xmax = gt_bboxes(im_no,3); gt_ymax = gt_bboxes(im_no,4);
        gt_width = gt_xmax - gt_xmin;
        gt_height = gt_ymax - gt_ymin;
        gt_fmtd = [gt_xmin, gt_ymin, gt_width, gt_height];
        
        intersectionArea = rectint(gt_fmtd,pr_fmtd);
        unionCoords = [min(gt_xmin,pr_xmin), ...
                       min(gt_ymin,pr_ymin), ...
                       max(gt_xmin + gt_width - 1, pr_xmin + pr_width - 1), ...
                       max(gt_ymin + gt_height - 1,pr_ymin + pr_height - 1)];
        unionArea = (unionCoords(3)-unionCoords(1)+1)*(unionCoords(4)-unionCoords(2)+1);
        %This should be greater than 0.5 to consider it as a valid detection.
        iou = intersectionArea / unionArea
        % if positive, label = +1. else -1
        if (iou > 0.5)
            labels(pb) = 1;
        else
            labels(pb) = -1;
        end
        scores(pb) = pr_bboxes(pb,5);
    end
    
    %[scores,s_idx] = sort(scores,'descend');
    %labels = labels(s_idx);
    % vl_pr(labels,scores); hold on;
    % save the values for future plots
    %save_path = '/home/isit/workspace/DefoObjInWild/results_final/';
    %fname = strcat(save_path,scenario,'_',cond, '_', diff, '_', loop_iter);
    %save(fname, 'labels','scores');
    %[recall,precision,info] = vl_pr(labels,scores);
end