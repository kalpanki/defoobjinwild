

function [res_img] = do_focus_blur(img, fh, fw)
% res_img - image after filtering
% img - input image
% fh - height of the kernel used for filtering
% fw - width of kernel used for filtering

    blur_type = 'focus';
    if strcmp(blur_type,'focus')
        h = fspecial('average',[fh fw]);
    end
    if strcmp(blur_type,'gaussian')
        h = fspecial('gaussian', 7);
    end

    res_img = imfilter(img,h,'replicate');

    % for o = 1:1
    %     img_names = dir(fullfile(img_src, strcat('object', int2str(o), '_img11*.jpg')));
    %     for i = 1:length(img_names)
    %         temp_img = imread(strcat(img_src, img_names(i).name));
    %         res_img = imfilter(temp_img,h,'replicate');
    %         % save in destination
    %         imwrite(res_img, strcat(img_dest, '/', img_names(i).name), 'jpg');
    %     end

end