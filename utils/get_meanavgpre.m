function [mean_avg_pre] = get_meanavgpre(gt, pr, num_classes)
    
    load(gt);
    load(pr);
    % get rid of numpy hack
    pr_bboxes = pr_bboxes(2:end,:);
    ap = [];
    
    % compute AP for each class first
    for cl = 1:num_classes
        pr_bboxes_cls = pr_bboxes(pr_bboxes(:,6) == cl,:); 
        confidence = pr_bboxes_cls(:,5);
        [sc,si]=sort(-confidence);
        nd = length(pr_bboxes_cls);
        tp=zeros(nd,1); 
        fp=zeros(nd,1);
        
        for d = 1:nd
            % one predicted box at a time based on sort index
            curr_pr_bbox = pr_bboxes_cls(si(d),:); 
            % get ground truth correspnding to this detection
            gt_box_idx = pr_bboxes_cls(si(d),7);
            curr_gt_bbox = gt_bboxes(gt_box_idx,:);
            % format for IoU computation
            pr_xmin = curr_pr_bbox(:,1); pr_ymin = curr_pr_bbox(:,2); 
            pr_xmax = curr_pr_bbox(:,3); pr_ymax = curr_pr_bbox(:,4);
            pr_width = pr_xmax - pr_xmin;
            pr_height = pr_ymax - pr_ymin;
            pr_fmtd = [pr_xmin, pr_ymin, pr_width, pr_height];
            % assumed to be only one, so take all
            gt_xmin = curr_gt_bbox(:,1); gt_ymin = curr_gt_bbox(:,2);
            gt_xmax = curr_gt_bbox(:,3); gt_ymax = curr_gt_bbox(:,4);
            gt_width = gt_xmax - gt_xmin;
            gt_height = gt_ymax - gt_ymin;
            gt_fmtd = [gt_xmin, gt_ymin, gt_width, gt_height];
            % compute intersection and union area
            intersectionArea = rectint(gt_fmtd,pr_fmtd);
            unionCoords = [min(gt_xmin,pr_xmin), ...
                   min(gt_ymin,pr_ymin), ...
                   max(gt_xmin + gt_width - 1, pr_xmin + pr_width - 1), ...
                   max(gt_ymin + gt_height - 1,pr_ymin + pr_height - 1)];
            unionArea = (unionCoords(3)-unionCoords(1)+1)*(unionCoords(4)-unionCoords(2)+1);
            % compute IoU scores
            iou = intersectionArea / unionArea;
            
            if (iou > 0.5)
                % true positive
                tp(d) = 1;
            end
            if (iou <= 0.5)
                % false positive
                fp(d) = 1;
            end
            
        end
        
        % compute precision/recall
        gt_bboxes_cls = gt_bboxes(gt_bboxes(:,5) == cl-1);
        npos = length(gt_bboxes_cls);
        fp=cumsum(fp);
        tp=cumsum(tp);
        rec=tp/npos;
        prec=tp./(fp+tp);
        % 
        ap = [ap VOCap(rec,prec)];
    end
    mean_avg_pre = mean(ap);
end