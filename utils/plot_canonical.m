clc; clear all; close all;

%% increasing number of iterations plot
load('/home/shrinivasan/work/defoobjinwild/dataset/results_canonical/map_scores_canonical_iters.mat');
figure;
x = [2000:2000:40000]
h_line = plot(x,[map,0.7810],'LineWidth',4); 
set(h_line,'Color', [0.75 0.75 1]);
h_dots = plot(x,[0,map], 'Marker', 'o', 'MarkerSize', 6, 'MarkerFaceColor' , [0.75 0.75 1]);
ylim([0 1]);

hXLabel = xlabel('Training Iterations');
hYLabel = ylabel('mAP Score');
set([hXLabel, hYLabel], 'FontName'   , 'AvantGarde');
set([hXLabel, hYLabel], 'FontSize', 13);
set(gca,'YGrid', 'on');
set(gca,'XGrid', 'on');

%% increasing no. of samples

load('/home/shrinivasan/work/defoobjinwild/dataset/results_canonical/map_scores_canonical_samples.mat');
figure;
x = [0:250:2500]
h1_line = plot(x,[0,map],'LineWidth',4); 
set(h1_line,'Color', [0.75 0.75 1]);
h_dots = plot(x,[0,map], 'Marker', 'o', 'MarkerSize', 6, 'MarkerFaceColor' , [0.75 0.75 1]);
ylim([0 1]);

h1XLabel = xlabel('Training Samples');
h1YLabel = ylabel('mAP Score');
set([h1XLabel, h1YLabel], 'FontName'   , 'AvantGarde');
set([h1XLabel, h1YLabel], 'FontSize', 13);
set(gca,'YGrid', 'on');
set(gca,'XGrid', 'on');

