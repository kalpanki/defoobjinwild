#!/home/isit/anaconda2/bin/python

import bpy
import bmesh
import math,os
import random
import sys


def getMaterial(object_no):
	# create a new 3x1 image 
	# TOSET: Set the object number 
	object_file = "/home/isit/workspace/DefoObjInWild/dataset/objects/object" + str(object_no) + ".jpg"
	img_path = os.path.expanduser(object_file)
	i1 = bpy.data.images.load(img_path)

	# if we don't pack the image into the .blend, it will be lost when you re-load the project
	i1.pack(True)

	# create a texture for the image
	t1 = bpy.data.textures.new("painting", 'IMAGE')
	t1.image = i1

	# give it sharp uninterpolated edges.
	t1.use_interpolation = False
	t1.filter_type = 'BOX'

	# put the texture on a material with UV coordinates
	m1 = bpy.data.materials.new('painting')
	m1.specular_intensity = 0.0
	m1.texture_slots.add()
	m1.texture_slots[0].texture = t1
	m1.texture_slots[0].texture_coords = 'UV'
	m1.texture_slots[0].uv_layer = 'spiral'

	return m1



def process_meshes(num_objects, meshes_per_obj, condition, difficulty):

	curr_mesh = 0
	# process each mesh one by one
	for co in range(num_objects):
		#for cm in range(meshes_per_obj):
		for cm in range(meshes_per_obj):
			f_name = "/home/isit/workspace/DefoObjInWild/dataset/temp_meshes/mesh" + \
						str(curr_mesh) + ".obj"
			bpy.ops.import_scene.obj(filepath=f_name)
			# choose the mesh as object
			# 3 - if there are 2 lamps
			# 2 - if there is 1 lamp
			obj = bpy.data.objects[3] 
			# let the mesh be rendered only one side, like in our printouts
			if obj.type == 'MESH':
				bpy.data.objects[3].data.show_double_sided = False
			else:
				bpy.ops.error.message('INVOKE_DEFAULT', type = "Error",
										message = 'Chosen object is not a MESH')

			###### Hard cases simulation ########
			# pose change
			if condition == 'po':
				# set min and max range based on difficulty
				if difficulty == 'easy':
					min_l = -0.5; max_l = 0.5
					min_r = -0.25; max_r = 0.25
				if difficulty == 'medium':
					min_l = -1; max_l = 1
					min_r = -0.5; max_r = 0.5
				if difficulty == 'hard':
					min_l = -2.0; max_l = 2.0
					min_r = -1.0; max_r = 1.0
				new_lx = random.uniform(min_l, max_l)
				new_ly = random.uniform(min_l, max_l)
				new_lz = random.uniform(min_l, max_l)
				obj.location.xyz = (new_lx,new_ly,new_lz)
				new_rx = random.uniform(min_r, max_r)
				new_ry = random.uniform(min_r, max_r)
				new_rz = random.uniform(min_r, max_r)
				obj.rotation_euler = (new_rx, new_ry, new_rz)
			
			#if wrinkles:
			
			# lighting change
			if condition == 'li':
				e = random.uniform(0.,10.0)
				for k in range(len(bpy.data.lamps)):
					bpy.data.lamps[k].energy = e

			# scale change or Field of View
			if condition == 'fv':
				# default case
				scale_small = 1.0
				scale_large = 1.0
				if difficulty == 'easy':
					scale_small = random.uniform(0.75,1.0)
					scale_large = random.uniform(1.0,1.5)
				if difficulty == 'medium':
					scale_small = random.uniform(0.5,0.75)
					scale_large = random.uniform(1.5,2.25)
				if difficulty == 'hard':
					scale_small = random.uniform(0.1,0.5)
					scale_large = random.uniform(2.25,3.0)
				
				if cm % 2 == 0:
					obj.scale = (scale_small,scale_small,scale_small)
				else:
					obj.scale = (scale_large,scale_large,scale_large)

			###### End of hard cases ############

			# get the material to append
			mat = getMaterial( co + 1 )
			obj.data.materials.append(mat)
			# let the lighting be bright
			#bpy.data.lamps[0].type = 'SUN'
			# render the scene
			# TOSET: Set the object number
			render_f_name = "/home/isit/workspace/DefoObjInWild/dataset/temp_rendered/object" \
							+ str(co) \
							+ '_' + str(cm) + ".jpg"
			bpy.data.scenes['Scene'].render.filepath = render_f_name
			bpy.data.scenes['Scene'].render.resolution_y = 1024
			bpy.data.scenes['Scene'].render.resolution_x = 1024
			bpy.ops.render.render( write_still=True )

			# remove the mesh
			bpy.data.objects[3].select = True
			bpy.ops.object.delete()

			curr_mesh += 1



if __name__ == "__main__":
	# initialise some stuff
	# num_meshes = 5
	#sys.path.append('/home/isit/anaconda2/lib/python2.7/site-packages/joblib')
	#from joblib import Parallel, delayed
	#import multiprocessing

	difficulty = sys.argv[-1]
	condition = sys.argv[-2]
	meshes_per_obj = int(sys.argv[-3])
	num_objects = int(sys.argv[-4])

	print ("Condition is: ", condition)

	#object_no = 1
	# different imaging conditions to incorporate:
	pose_change = False
	wrinkles = False
	lighting_change = False
	scale_change = False

	# num_cores = multiprocessing.cpu_count()
	#Parallel(n_jobs = num_cores)(delayed(process_meshes)(num_objects, i) for i in range(meshes_per_obj))

	process_meshes(num_objects, meshes_per_obj, condition, difficulty)

