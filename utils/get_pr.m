function [avg_pre] = get_pr(gt, pr)

    load(gt);
    load(pr);
    % get rid of numpy hack
    pr_bboxes = pr_bboxes(2:end,:);
    addpath(genpath('/home/shrinivasan/work/vlfeat/toolbox'));
    vl_setup;
    labels = []; scores = [];
    % iterate all ground truth images (each image has only one gt box)
    for gb = 1:size(gt_bboxes,1)
        labels_new = []; scores_new = [];
        % get detections corresponding to image considered
        curr_pr_bboxes = pr_bboxes(find(pr_bboxes(:,7) == gb-1),:);
        % sort by scores
        [~,idx] = sort(curr_pr_bboxes(:,5),'descend');
        curr_pr_bboxes = curr_pr_bboxes(idx,:);
        % iterate detections per image
        for j = 1:size(curr_pr_bboxes,1)
            % is object is not detected, just make score -inf
            if (size(curr_pr_bboxes,1) == 1 && curr_pr_bboxes(j,6) == 1)
                labels = [labels 1];
                scores = [scores -inf];
            else
                % compute IoU score
                pr_xmin = curr_pr_bboxes(j,1); pr_ymin = curr_pr_bboxes(j,2); 
                pr_xmax = curr_pr_bboxes(j,3); pr_ymax = curr_pr_bboxes(j,4);
                pr_width = pr_xmax - pr_xmin;
                pr_height = pr_ymax - pr_ymin;
                pr_fmtd = [pr_xmin, pr_ymin, pr_width, pr_height];
                gt_xmin = gt_bboxes(gb,1); gt_ymin = gt_bboxes(gb,2);
                gt_xmax = gt_bboxes(gb,3); gt_ymax = gt_bboxes(gb,4);
                gt_width = gt_xmax - gt_xmin;
                gt_height = gt_ymax - gt_ymin;
                gt_fmtd = [gt_xmin, gt_ymin, gt_width, gt_height];
                % compute intersection and union area
                intersectionArea = rectint(gt_fmtd,pr_fmtd);
                unionCoords = [min(gt_xmin,pr_xmin), ...
                               min(gt_ymin,pr_ymin), ...
                               max(gt_xmin + gt_width - 1, pr_xmin + pr_width - 1), ...
                               max(gt_ymin + gt_height - 1,pr_ymin + pr_height - 1)];
                unionArea = (unionCoords(3)-unionCoords(1)+1)*(unionCoords(4)-unionCoords(2)+1);
                % finally, compute IoU scores
                iou = intersectionArea / unionArea;
                % if positive, label = +1. else -1
                if (iou > 0.5)
                    labels = [labels 1];
                    scores = [scores curr_pr_bboxes(j,5)];
                    %scores = [scores 0.1];
                else
                    labels = [labels -1];
                    scores = [scores curr_pr_bboxes(j,5)];
                    %scores = [scores 0.1];
                end
            end
        end
    end
    sum(scores == -inf)
    % vl_pr(labels,scores);
    [recall,precision,info] = vl_pr(labels,scores);
    avg_pre = info.ap;
end