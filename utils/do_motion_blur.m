function [res_img] = do_motion_blur(img, len, theta)
% len - distance in pixels
% theta - angle of motion

    h = fspecial('motion', len, theta);
    res_img = imfilter(img,h,'replicate');

end