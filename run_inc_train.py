#!/home/isit/anaconda2/bin/python

'''
Runs modified fast R-CNN algorithm for detection.
The approach is: 
1. Generate test data for a single imaging condition.
2. Train incrementally (in a loop) for the chosen imaging condition, one at a time.
3. Test using the generated in the loop.
'''

from global_vars import *
import os
from generator import *
from trainer import *
from incrementor import *
from detector import *
from evaluator import *
import scipy.io as sio
import shutil

if G_IS_LOGGING:
	logFile = ' >> output.log'
else:
	logFile = ' > /dev/null 2>&1'

###############################################################################
## some initializations
###############################################################################
completed = False
accuracies = []
easy_accuracies = []
medium_accuracies = []
hard_accuracies = []
delOrder = ['no', 'easy', 'medium', 'hard']
# A common incrementor object to use in our loop
commonInc = Incrementor(delOrder, logFile)
commonInc.curr_del = 'no'
doInitTraining = True
doGenerateTestData = False

###############################################################################
## GENERATE INITIAL TRAINING MODEL USING IMAGENET INITIALIZATION
###############################################################################
if doInitTraining:
	mode = 'train'
	isInLoop = False
	totalMeshes = G_NUM_OBJECTS * G_MESHES_PER_OBJ
	currGen = Generator(totalMeshes, mode, commonInc.curr_del, isInLoop, logFile)
	currGen.generate_data()
	# currGen.generate_proposals()

	totalMeshes = G_NUM_OBJECTS * G_MESHES_PER_OBJ
	mode = 'train'
	initModelFile = '/home/isit/workspace/fast-rcnn/data/imagenet_models/CaffeNet.v2.caffemodel'
	tr = Trainer(initModelFile, logFile)
	# let the initial model be trained for 40000 iterations
	latestModel = tr.train(40000)

###############################################################################
## Get the latest trained model to be used
###############################################################################
def get_latest_model():
	os.chdir(G_DETECTOR_PATH + '/output/default/train/')
	cmd = 'ls -t1 | head -n 1'
	proc = subprocess.Popen(cmd, stdout=subprocess.PIPE,shell=True)
	(op,err) = proc.communicate()
	op = op.rstrip('\n')
	op = G_DETECTOR_PATH + '/output/default/train/' + op
	return op

###############################################################################
## Main feedback loop
###############################################################################
k = 0
while not completed:
	# generate training data
	mode = 'train'
	isInLoop = True
	totalMeshes = G_NUM_OBJECTS * G_LOOP_MESHES_PER_OBJ
	currGen = Generator(totalMeshes, mode, commonInc.curr_del, isInLoop, logFile)
	currGen.generate_data()
	del currGen

	# train using the training data
	latestModel = get_latest_model()
	print ("Going to TRAIN with model initialization %s " % latestModel)
	tr = Trainer(latestModel, logFile)
	latestModel = tr.train(2000)
	newFile = latestModel.strip('.caffemodel') + '_' + str(k) + '.caffemodel'
	print ("Latest model is: ", newFile)
	shutil.copy(latestModel,newFile)
	del latestModel
	del tr
	k += 1

	# Detect using the newly trained model
	testPrototxtFile = G_DETECTOR_PATH + '/models/DefoObject/' + 'test.prototxt'
	latestModel = get_latest_model()
	print ("Going to init Detector using the model: ", latestModel)
	currDetector = Detector(latestModel, testPrototxtFile, logFile)
	predictResMatFile = currDetector.detect()
	del latestModel
	del currDetector

	# Evaluate detection
	gtMatFile =  '/home/isit/workspace/DefoObjInWild/dataset/gt_bboxes.mat'
	currEval = Evaluator(gtMatFile, predictResMatFile, logFile)
	status = currEval.evaluate()

	completed, temp_accuracy = commonInc.check_results()
	print ("ACCURACY in the iteration is: %s with difficulty: %s " % (temp_accuracy, commonInc.curr_del))

	# append to right array for storing
	if commonInc.curr_del == 'no':
		accuracies.append(temp_accuracy)
	if commonInc.curr_del == 'easy':
		easy_accuracies.append(temp_accuracy)
	if commonInc.curr_del == 'medium':
		medium_accuracies.append(temp_accuracy)
	if commonInc.curr_del == 'hard':
		hard_accuracies.append(temp_accuracy)

	# store the computed accuracies
	sio.savemat('easy_accuracy.mat',{'accuracy' : easy_accuracies})
	sio.savemat('easy_accuracy.mat',{'accuracy' : easy_accuracies})
	sio.savemat('medium_accuracy.mat',{'accuracy' : medium_accuracies})
	sio.savemat('hard_accuracy.mat',{'accuracy' : hard_accuracies})


