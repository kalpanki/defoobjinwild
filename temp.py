from global_vars import *
import os
from generator import *
from trainer import *
from incrementor import *
from detector import *
from evaluator import *
import scipy.io as sio
import shutil


# some initializations
completed = False
fb_map = []
mb_map = []
po_map = []
de_map = []
eo_map = []
fv_map = []
logFile = ' > output.log'
delOrder = ['easy', 'medium', 'hard']
# A common incrementor object to use in our loop
commonInc = Incrementor(delOrder, logFile)
commonInc.curr_del = 'easy'
doGenerateTestData = True
# to store the models at each iteration
k = 'test'
imgConditions = ['fb']

# useful function
def get_latest_model():
	os.chdir(G_DETECTOR_PATH + '/output/default/train/')
	cmd = 'ls -t1 | head -n 1'
	proc = subprocess.Popen(cmd, stdout=subprocess.PIPE,shell=True)
	(op,err) = proc.communicate()
	op = op.rstrip('\n')
	op = G_DETECTOR_PATH + '/output/default/train/' + op
	return op


# iteratively train for simple imaging condition
for cond in imgConditions:
	# while not completed:
	for currDel in delOrder:
		print ("Running Imaging Condition: %s with difficulty: %s " % (cond, currDel))
		commonInc.curr_del = currDel

		# generate train data
		# mode = 'train'
		# isInLoop = True
		# print ("\nGenerating train data")
		# totalMeshes = G_LOOP_MESHES_PER_OBJ
		# currGen = Generator(totalMeshes, mode, commonInc.curr_del, isInLoop, k, cond, logFile)
		# currGen.generate_data(False)
		# currGen = 0

		# TO RUN: generate test data
		mode = 'test'
		isInLoop = True
		print ("\nGenerating test data")
		totalMeshes = G_LOOP_MESHES_PER_OBJ
		currGen = Generator(totalMeshes, mode, commonInc.curr_del, isInLoop, k, cond, logFile)
		currGen.generate_data(False)
