#!/home/isit/anaconda2/bin/python

from global_vars import *
import os
from generator import *
from trainer import *
from incrementor import *
from detector import *
from evaluator import *
import scipy.io as sio
import shutil

# some initializations
completed = False
logFile = ' > output.log'
imgConditions = ['fb', 'mb', 'po', 'de', 'eo', 'fv']
delOrder = ['easy', 'medium', 'hard']
# imgConditions = ['no']
# delOrder = ['no']
# A common incrementor object to use in our loop
commonInc = Incrementor(delOrder, logFile)
commonInc.curr_del = 'easy'
# to store the models at each iteration
k = 1 

# useful function
def get_latest_model():
	os.chdir(G_DETECTOR_PATH + '/output/default/train/')
	cmd = 'ls -t1 | head -n 1'
	proc = subprocess.Popen(cmd, stdout=subprocess.PIPE,shell=True)
	(op,err) = proc.communicate()
	op = op.rstrip('\n')
	op = G_DETECTOR_PATH + '/output/default/train/' + op
	return op


mean_avg_prec = []
# test using the data
testPrototxtFile = G_DETECTOR_PATH + '/models/DefoObject/' + 'test.prototxt'
testProposalsFile = '/home/isit/workspace/DefoObjInWild/dataset/test_proposals.mat'
for i in range(10):
	latestModel = '/home/isit/workspace/fast-rcnn/output/default/train/defo_obj_fast_rcnn_iter_' + str(i+1) + '0000.caffemodel'
	print ("Going to init Detector using the model: ", latestModel)
	currDetector = Detector(latestModel, testPrototxtFile, testProposalsFile, i, logFile)
	predictResMatFile = currDetector.detect()
	

