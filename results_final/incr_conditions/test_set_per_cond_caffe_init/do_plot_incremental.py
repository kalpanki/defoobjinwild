#!/home/isit/anaconda2/bin/python

import plotly.graph_objs as go
import plotly.plotly as py
from plotly.offline import plot
import scipy.io as sio
import numpy as np

plt_title = 'Scale Change'
cond = 'fv'

map_mat_easy = sio.loadmat('map_%s_easy.mat' % cond)
map_mat_easy = map_mat_easy['map']
map_mat_easy = np.squeeze(map_mat_easy)
map_mat_med = sio.loadmat('map_%s_medium.mat' % cond)
map_mat_med = map_mat_med['map']
map_mat_med = np.squeeze(map_mat_med)
map_mat_hard = sio.loadmat('map_%s_hard.mat' % cond)
map_mat_hard = map_mat_hard['map']
map_mat_hard = np.squeeze(map_mat_hard)

# trace_acc = go.Scatter(y = acc_mat, name='Accuracy')
trace_map_easy = go.Scatter(x = range(1,len(map_mat_easy+1)), y = map_mat_easy, name='Easy')
trace_map_med = go.Scatter(x = range(1, len(map_mat_med+1)), y = map_mat_med, name='Medium')
trace_map_hard = go.Scatter(x = range(1, len(map_mat_hard+1)), y = map_mat_hard, name='Hard')

data = [trace_map_easy, trace_map_med, trace_map_hard]

layout = go.Layout(autosize=False, margin=go.Margin(l = 55,r=50, b=45,t=45),
									  title = plt_title, 
									  yaxis = dict(range=[0,1],title='AP Score', titlefont=dict(size=22,color='black'),tickfont=dict(size=18,color='black')), 
									  xaxis = dict(titlefont=dict(size=22,color='black'),title='Iterations of the loop, k',tickfont=dict(size=18,color='black')),
									  showlegend=True, legend = dict(x=0.9,y=1), font=dict(size=18,color='black'))
# layout = go.Layout(xaxis=dict(ticktext=["Easy","Medium","Hard"],tickvals=[1,2,3]))
fig = go.Figure(data=data,layout=layout)
plot(fig)
