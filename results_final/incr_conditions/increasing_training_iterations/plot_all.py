#!/home/isit/anaconda2/bin/python

import plotly.graph_objs as go
import plotly.plotly as py
from plotly.offline import plot
import scipy.io as sio
import numpy as np

map_mat_easy = sio.loadmat('res_till_10000.mat')
map_mat_easy = map_mat_easy['map']
map_mat_easy = np.squeeze(map_mat_easy)

#trace_map_easy = go.Scatter(x = range(10000,110000,10000), y = map_mat_easy)
trace_map_easy = go.Scatter(x = range(1000,10000,1000), y = map_mat_easy)
data = [trace_map_easy]

layout = go.Layout(autosize=False, margin=go.Margin(l = 55,r=50, b=45,t=45),
					# title = 'Performance after 10000 iterations',
					xaxis = dict(titlefont=dict(size=21,color='black'),title='Training Iterations',tickfont=dict(size=18,color='black')),
					yaxis=dict(titlefont=dict(size=21,color='black'),range=[0,1],title='AP Score',tickfont=dict(size=18,color='black')), 
					showlegend=False)

fig = go.Figure(data=data, layout = layout)
plot(fig)