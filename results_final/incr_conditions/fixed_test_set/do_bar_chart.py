#!/home/isit/anaconda2/bin/python

import plotly.graph_objs as go
import plotly.plotly as py
from plotly.offline import plot
import scipy.io as sio
import numpy as np

cond = 'fv'
# read all values
map_mat_easy = sio.loadmat('map_%s_easy.mat' % cond)
map_mat_easy = map_mat_easy['map']
map_mat_easy = np.squeeze(map_mat_easy)
map_mat_med = sio.loadmat('map_%s_medium.mat' % cond)
map_mat_med = map_mat_med['map']
map_mat_med = np.squeeze(map_mat_med)
map_mat_hard = sio.loadmat('map_%s_hard.mat' % cond)
map_mat_hard = map_mat_hard['map']
map_mat_hard = np.squeeze(map_mat_hard)


# iterate each and append the max values to arrays: easy_scores, medium_scores, hard_scores
conds = ['fb', 'mb', 'po', 'de', 'eo', 'fv']
diffs = ['easy','medium', 'hard']

easy_scores = []; medium_scores = []; hard_scores = []

for dif in diffs:
	for cond in conds:
		if dif == 'easy':
			map_mat = sio.loadmat('map_%s_easy.mat' % cond)
			map_mat = map_mat['map']
			map_mat = np.squeeze(map_mat)
			easy_scores.append(max(map_mat))
		if dif == 'medium':
			map_mat = sio.loadmat('map_%s_medium.mat' % cond)
			map_mat = map_mat['map']
			map_mat = np.squeeze(map_mat)
			medium_scores.append(max(map_mat))
		if dif == 'hard':
			map_mat = sio.loadmat('map_%s_hard.mat' % cond)
			map_mat = map_mat['map']
			map_mat = np.squeeze(map_mat)
			hard_scores.append(max(map_mat))

print "Easy scores are:"
print easy_scores
print "Medium scores are:"
print medium_scores
print "Hard scores are:"
print hard_scores


trace1 = go.Bar(
    x=['Focus Blur', 'Motion Blur', 'Pose', 'Deformations', 'Occlusions', 'Scale'],
    y=easy_scores,
    name='Easy'
)

trace2 = go.Bar(
    x=['Focus Blur', 'Motion Blur', 'Pose', 'Deformations', 'Occlusions', 'Scale'],
    y=medium_scores,
    name='Medium',
    marker=dict(
    	color='rgb(142, 124, 195)'
    )
)

trace3 = go.Bar(
    x=['Focus Blur', 'Motion Blur', 'Pose', 'Deformations', 'Occlusions', 'Scale'],
    y=hard_scores,
    name='Hard',
    marker=dict(
        color='rgb(204,204,204)'
    )
)

data = [trace1, trace2, trace3]
layout = go.Layout(
	title='Discarding Past Images',	
	yaxis=dict(title='AP Score'),
	#autosize=False, margin=go.Margin(l = 25,r=0, b=75,t=25),
	xaxis=dict(tickangle=-20,titlefont=dict(size=24,color='black'),tickfont=dict(size=20,color='black')),
	# yaxis=dict(titlefont=dict(size=22,color='black')),
    barmode='group',
    font=dict(size=24,color='black')
)

fig = go.Figure(data=data,layout=layout)
plot(fig)