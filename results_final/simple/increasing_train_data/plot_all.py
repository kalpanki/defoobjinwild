#!/home/isit/anaconda2/bin/python

import plotly.graph_objs as go
import plotly.plotly as py
from plotly.offline import plot
import scipy.io as sio
import numpy as np

map_mat_easy = sio.loadmat('res_map.mat')
map_mat_easy = map_mat_easy['map']
map_mat_easy = np.squeeze(map_mat_easy)

trace_map_easy = go.Scatter(x = range(250,2750,250), y = map_mat_easy)
data = [trace_map_easy]

layout = go.Layout(autosize=False, margin=go.Margin(l = 55,r=50, b=45,t=45),
					#title = 'Increasing Training Samples',
					xaxis = dict(titlefont=dict(size=21,color='black'),
								 #range = range(0,2750,250),
								 title='Training Samples',
								 color='black',
								 tickfont=dict(size=18,color='black'),
								 gridwidth=2),
					yaxis=dict(titlefont=dict(size=21,color='black'),
							   range=[0,1],
							   title='AP Score',
							   color='black',
							   tickfont=dict(size=18,color='black'),
							   gridwidth=2), 
					showlegend=False)

fig = go.Figure(data=data, layout = layout)
plot(fig)