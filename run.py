#!/home/isit/anaconda2/bin/python

from global_vars import *
import os
from generator import *
from trainer import *
from incrementor import *
from detector import *
from evaluator import *
import scipy.io as sio
import shutil

if G_IS_LOGGING:
	logFile = ' >> output.log'
else:
	logFile = ' > /dev/null 2>&1'

###############################################################################
## some initializations
###############################################################################
completed = False
accuracies = []
easy_accuracies = []
medium_accuracies = []
hard_accuracies = []
delOrder = ['no', 'easy', 'medium', 'hard']
# A common incrementor object to use in our loop
commonInc = Incrementor(delOrder, logFile)
commonInc.curr_del = 'easy'
doInitTraining = False
doGenerateTestData = False
###############################################################################
## Generate training data, Train with ImageNet as initial model
###############################################################################
if doInitTraining:
	mode = 'train'
	isInLoop = False
	totalMeshes = G_NUM_OBJECTS * G_MESHES_PER_OBJ
	currGen = Generator(totalMeshes, mode, currentDelta, isInLoop, logFile)
	currGen.generate_data()
	# currGen.generate_proposals()

	totalMeshes = G_NUM_OBJECTS * G_MESHES_PER_OBJ
	mode = 'train'
	initModelFile = '/home/isit/workspace/fast-rcnn/data/imagenet_models/CaffeNet.v2.caffemodel'
	tr = Trainer(initModelFile, logFile)
	# let the initial model be trained for 40000 iterations
	latestModel = tr.train(40000)


###############################################################################
## Get the latest trained model to be used
###############################################################################
def get_latest_model():
	os.chdir(G_DETECTOR_PATH + '/output/default/train/')
	cmd = 'ls -t1 | head -n 1'
	proc = subprocess.Popen(cmd, stdout=subprocess.PIPE,shell=True)
	(op,err) = proc.communicate()
	op = op.rstrip('\n')
	op = G_DETECTOR_PATH + '/output/default/train/' + op
	return op

# loop through and train for imaging conditions
k = 1
while not completed:

	# Generate training data and train to improve accuracy for the newly chosen difficulty
	mode = 'train'
	isInLoop = True
	totalMeshes = G_NUM_OBJECTS * G_LOOP_MESHES_PER_OBJ
	currGen = Generator(totalMeshes, mode, currentDelta, isInLoop, logFile)
	currGen.generate_data()
	# currGen.generate_proposals()
	del currGen

	# train with new data
	latestModel = get_latest_model()
	tr = Trainer(latestModel, logFile)
	print ('\n')
	print ("#############################################")
	print ("Going to TRAIN with model initialization %s " % latestModel)
	print ("#############################################")
	print ('\n')
	latestModel = tr.train(G_LOOP_TRAINING_ITERATIONS)
	print ("Latest model is: ", latestModel)

	# Generate test data for the detector to work upon
	# should store / handle GT bounding boxes - gt_boxes.mat
	if doGenerateTestData:
		print ("GENERATING TEST DATA: ")
		mode = 'test'
		isInLoop = True
		total_test_meshes = G_NUM_OBJECTS * G_TEST_M
		currGen = Generator(total_test_meshes, mode, currentDelta, isInLoop, logFile)
		currGen.generate_data()
		# currGen.generate_proposals()
		currGen.clean_temp_directories()
		del currGen

	# Run detection using the trained model (*.caffemodel file)
	# and get the bounding boxes saved as mat file
	print ("RUNNING DETECTION ON NEW TEST DATA: ")
	testPrototxtFile = G_DETECTOR_PATH + '/models/DefoObject/' + 'test.prototxt'
	# testProposalFile = '/home/isit/workspace/DefoObjInWild/dataset/test_proposals.mat'
	latestModel = get_latest_model()
	print ('\n')
	print ("#############################################")
	print ("Going to TEST with the model %s " % latestModel)
	print ("#############################################")
	print ('\n')
	currDetector = Detector(latestModel, testPrototxtFile, logFile)
	# mat file contatinig the detected bounding boxes
	predictResMatFile = currDetector.detect()

	# Evaluate the detected results
	print ("EVALUATING DETECTION RESULT TO GENERATE IoU Scores: ")
	gtMatFile =  '/home/isit/workspace/DefoObjInWild/dataset/gt_bboxes.mat';
	currEval = Evaluator(gtMatFile, predictResMatFile, logFile)
	status = currEval.evaluate()

	# increment or stay at same level based on test results
	print ("CHECKING INCREMENTOR For Accuracy and difficulty: ")
	oldDelta = commonInc.curr_del
	completed, temp_accuracy = commonInc.check_results()
	print ("ACCURACY in the iteration is: %s with difficulty: %s " % (temp_accuracy, commonInc.curr_del))

	# we generate new test data if the difficulty level changes
	if not oldDelta == commonInc.curr_del:
		print ("Setting generate test data to True")
		doGenerateTestData = True
	else:
		doGenerateTestData = False

	# which list to append the accuracies
	if commonInc.curr_del == 'no':
		accuracies.append(temp_accuracy)
	if commonInc.curr_del == 'easy':
		easy_accuracies.append(temp_accuracy)
	if commonInc.curr_del == 'medium':
		medium_accuracies.append(temp_accuracy)
	if commonInc.curr_del == 'hard':
		hard_accuracies.append(temp_accuracy)

	# save so that we can examine in matlab
	sio.savemat('accuracy.mat',{'accuracy' : accuracies})
	sio.savemat('easy_accuracy.mat',{'accuracy' : easy_accuracies})
	sio.savemat('medium_accuracy.mat',{'accuracy' : medium_accuracies})
	sio.savemat('hard_accuracy.mat',{'accuracy' : hard_accuracies})

print accuracies