#!/home/isit/anaconda2/bin/python

'''
Runs modified fast R-CNN algorithm for detection.
This will run the traditional way:
1. Generate test data for all conditions and difficulties
2. Generate train data for all conditions and difficulties
3. Train with all training data
4. Test with all testing data

Imaging conditions considered:
Focus Blur, Motion Blur, Pose Change, Deformation, Self Occlution, 
External Occlution, Field of View/Scale change.
'''

from global_vars import *
import os
from generator import *
from trainer import *
from incrementor import *
from detector import *
from evaluator import *
import scipy.io as sio
import shutil


# some initializations
completed = False
logFile = ' > output.log'
imgConditions = ['fb', 'mb', 'po', 'de', 'eo', 'fv']
delOrder = ['easy', 'medium', 'hard']
# A common incrementor object to use in our loop
commonInc = Incrementor(delOrder, logFile)
commonInc.curr_del = 'easy'
# to store the models at each iteration
k = 1 

# useful function
def get_latest_model():
	os.chdir(G_DETECTOR_PATH + '/output/default/train/')
	cmd = 'ls -t1 | head -n 1'
	proc = subprocess.Popen(cmd, stdout=subprocess.PIPE,shell=True)
	(op,err) = proc.communicate()
	op = op.rstrip('\n')
	op = G_DETECTOR_PATH + '/output/default/train/' + op
	return op

# generate test data for all condition+difficulty
# mode = 'test'
# isInLoop = True
# for cond in imgConditions:
# 	for currDel in delOrder:
# 		commonInc.curr_del = currDel
# 		print ("\nGenerating test data for condition: %s with difficulty: %s \n" % (cond, currDel))
# 		totalMeshes = G_IMGS_PER_TEST_CONDITION 
# 		currGen = Generator(totalMeshes, mode, commonInc.curr_del, isInLoop, k, cond, logFile)
# 		currGen.generate_data(False)
# currGen.generate_proposals()
# currGen = 0

# Delete Images, Annotations,
# ImageSets/train.txt 
# All trained models before running.
# generate training data for all cond+difficulty
mode = 'train'
isInLoop = True
for cond in imgConditions:
	for currDel in delOrder:
		print ("\nGenerating training data for condition: %s with difficulty: %s \n" % (cond, currDel))
		commonInc.curr_del = currDel
		# generate training data
		totalMeshes = G_LOOP_MESHES_PER_OBJ
		currGen = Generator(totalMeshes, mode, commonInc.curr_del, isInLoop, k, cond, logFile)
		# False - do not delete existing data
		currGen.generate_data(False)
currGen.generate_proposals()
currGen = 0


# train using the training data
latestModel = get_latest_model()
print ("Going to TRAIN with model initialization %s " % latestModel)
tr = Trainer(latestModel, logFile)
latestModel = tr.train(G_LOOP_TRAINING_ITERATIONS)
# newFile = latestModel.strip('.caffemodel') + '_' + str(k) + '.caffemodel'
# print ("Latest model is: %s " % newFile)
# shutil.copy(latestModel,newFile)
# del latestModel
# del tr

# test using the data - for each trained model - 10,000 - 100,000
testPrototxtFile = G_DETECTOR_PATH + '/models/DefoObject/' + 'test.prototxt'
testProposalsFile = '/home/isit/workspace/DefoObjInWild/dataset/test_proposals.mat'
for k in range(6,8):
	latestModel = '/home/isit/workspace/fast-rcnn/output/default/train/defo_obj_fast_rcnn_iter_' + str(k+1) + '000.caffemodel'
	print ("Going to init Detector using the model: ", latestModel)
	currDetector = Detector(latestModel, testPrototxtFile, testProposalsFile, k+1, 'no','no', logFile)
	predictResMatFile = currDetector.detect()
	# gtMatFile = currDetector.get_gt_data()

# EVALUATE MANUALLY
# Evaluate detection
# currEval = Evaluator(gtMatFile, predictResMatFile, logFile)
# status = currEval.evaluate()
# completed, accuracy, meanAvPr = commonInc.check_results()
# print ("MEAN IOU in the iteration is: %s with difficulty: %s " % (meanAvPr, commonInc.curr_del))
