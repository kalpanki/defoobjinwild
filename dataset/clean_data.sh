#!/usr/bin/env sh


# remove all train images
rm /home/isit/workspace/DefoObjInWild/dataset/Images/*
rm /home/isit/workspace/DefoObjInWild/dataset/Annotations/*

# remove ttest images
rm /home/isit/workspace/DefoObjInWild/dataset/TestImages/*
rm /home/isit/workspace/DefoObjInWild/dataset/TestAnnotations/*

# other files
rm /home/isit/workspace/DefoObjInWild/dataset/ImageSets/*.txt
rm /home/isit/workspace/DefoObjInWild/dataset/pr_bboxes.mat
rm /home/isit/workspace/DefoObjInWild/dataset/gt_bboxes.mat
rm /home/isit/workspace/DefoObjInWild/dataset/test_proposals.mat
rm /home/isit/workspace/DefoObjInWild/dataset/train_proposals.mat
rm /home/isit/workspace/DefoObjInWild/dataset/iou_scores.mat
