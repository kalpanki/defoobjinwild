#!/home/isit/anaconda2/bin/python

'''
Runs modified fast R-CNN algorithm for detection.
Generate TEST set first:
1. We iterate over a list of imaging conditions
2. Generate test images for each of them and keep all for testing:
	Ste <= Ste(condition) U Ste
Adding TRAINING conditions one after the other:
1. We iterate over a list of imaging conditions
2. Iteratively, For each difficulty in that imaging condition, we 
generate train images, Str <= Str(condition) U Str

and train till we reach a threshold

Imaging conditions considered:
Focus Blur, Motion Blur, Pose Change, Deformation, Self Occlution, 
External Occlution, Field of View/Scale change.
'''

from global_vars import *
import os
from generator import *
from trainer import *
from incrementor import *
from detector import *
from evaluator import *
import scipy.io as sio
import shutil

# some initializations
completed = False
fb_map = []
mb_map = []
po_map = []
de_map = []
eo_map = []
fv_map = []
logFile = ' > output.log'
delOrder = ['easy', 'medium', 'hard']
# A common incrementor object to use in our loop
commonInc = Incrementor(delOrder, logFile)
commonInc.curr_del = 'easy'
doGenerateTestData = True
# to store the models at each iteration
k = 1 
imgConditions = ['fb', 'mb', 'po', 'de', 'eo', 'fv']

# useful function
def get_latest_model():
	os.chdir(G_DETECTOR_PATH + '/output/default/train/')
	cmd = 'ls -t1 | head -n 1'
	proc = subprocess.Popen(cmd, stdout=subprocess.PIPE,shell=True)
	(op,err) = proc.communicate()
	op = op.rstrip('\n')
	op = G_DETECTOR_PATH + '/output/default/train/' + op
	return op

# generate test data for all imaging conditions
mode = 'test'
isInLoop = True
for cond in imgConditions:
	for currDel in delOrder:
		commonInc.curr_del = currDel
		print ("\nGenerating test data for condition: %s with difficulty: %s \n" % (cond, currDel))
		totalMeshes = G_LOOP_MESHES_PER_OBJ 
		currGen = Generator(totalMeshes, mode, commonInc.curr_del, isInLoop, k, cond, logFile)
		currGen.generate_data(False)
currGen.generate_proposals()

# iteratively train for each condition and difficulty
for cond in imgConditions:
	for currDel in delOrder:
		print ("Running for Imaging Condition: %s with difficulty: %s " % (cond, currDel))
		commonInc.curr_del = currDel

		# generate training data
		mode = 'train'
		isInLoop = True
		totalMeshes = G_LOOP_MESHES_PER_OBJ
		currGen = Generator(totalMeshes, mode, commonInc.curr_del, isInLoop, k, cond, logFile)
		# False - do not delete existing data
		currGen.generate_data(False)
		currGen.generate_proposals()
		currGen = 0

		# train using the training data
		latestModel = get_latest_model()
		print ("Going to TRAIN with model initialization %s " % latestModel)
		tr = Trainer(latestModel, logFile)
		latestModel = tr.train(G_LOOP_TRAINING_ITERATIONS)
		newFile = latestModel.strip('.caffemodel') + '_' + str(k) + '.caffemodel'
		print ("Latest model is: %s " % newFile)
		shutil.copy(latestModel,newFile)
		del latestModel
		del tr
		k += 1

		# Detect using the newly trained model
		testPrototxtFile = G_DETECTOR_PATH + '/models/DefoObject/' + 'test.prototxt'
		testProposalsFile = '/home/isit/workspace/DefoObjInWild/dataset/test_proposals.mat'
		latestModel = get_latest_model()
		print ("Going to init Detector using the model: ", latestModel)
		currDetector = Detector(latestModel, testPrototxtFile, testProposalsFile, logFile)
		predictResMatFile = currDetector.detect()
		gtMatFile = currDetector.get_gt_data()

		# Evaluate detection
		currEval = Evaluator(gtMatFile, predictResMatFile, logFile)
		status = currEval.evaluate()
		completed, accuracy, meanAvPr = commonInc.check_results()
		print ("MEAN IOU in the iteration is: %s with difficulty: %s " % (meanAvPr, commonInc.curr_del))

		# append to right arrays for storing
		if cond == 'fb':
			fb_map.append(meanAvPr)
		if cond == 'mb':
			mb_map.append(meanAvPr)
		if cond == 'po':
			po_map.append(meanAvPr)
		if cond == 'de':
			de_map.append(meanAvPr)
		if cond == 'eo':
			eo_map.append(meanAvPr)
		if cond == 'fv':
			fv_map.append(meanAvPr)
		sio.savemat('fb_map.mat', {'map' : fb_map})
		sio.savemat('mb_map.mat', {'map' : mb_map})
		sio.savemat('po_map.mat', {'map' : po_map})
		sio.savemat('de_map.mat', {'map' : de_map})
		sio.savemat('eo_map.mat', {'map' : eo_map})
		sio.savemat('fv_map.mat', {'map' : fv_map})