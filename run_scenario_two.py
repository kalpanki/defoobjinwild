#!/home/isit/anaconda2/bin/python

'''
Runs modified fast R-CNN algorithm for detection.
This script is to run the SCENARIO 2, which is:
1. Generate test data for all possible imaging conditions
2. Train incrementally (in a loop) for the single chosen imaging condition.
3. Grow the training data in each iteration by doing: 
		A(1)<= A(0) + new data.
4. Test using the generated in step 1. 

Incremental training is when we initialize training with the model obtained from previous loop.
'''


from global_vars import *
import os
from generator import *
from trainer import *
from incrementor import *
from detector import *
from evaluator import *
import scipy.io as sio
import shutil


logFile = ' >> output.log'

# some initializations
completed = False
accuracies = []
easy_accuracies = []
medium_accuracies = []
hard_accuracies = []
no_map = []
easy_map = []
medium_map = []
hard_map = []
delOrder = ['no', 'easy', 'medium', 'hard']
# A common incrementor object to use in our loop
commonInc = Incrementor(delOrder, logFile)
commonInc.curr_del = 'no'
doGenerateTestData = True
k = 1 # to store the models at each iteration
train_iter_mul = 1 # to increment the training iterations

# useful function
def get_latest_model():
	os.chdir(G_DETECTOR_PATH + '/output/default/train/')
	cmd = 'ls -t1 | head -n 1'
	proc = subprocess.Popen(cmd, stdout=subprocess.PIPE,shell=True)
	(op,err) = proc.communicate()
	op = op.rstrip('\n')
	op = G_DETECTOR_PATH + '/output/default/train/' + op
	return op


# generate test data
mode = 'test'
isInLoop = False
for i in range(len(delOrder)):
	print ("\nGenerating test data for delta: ", delOrder[i], "\n")
	totalMeshes = G_IMGS_PER_TEST_CONDITION 
	currGen = Generator(totalMeshes, mode, delOrder[i], isInLoop, k, logFile)
	currGen.generate_data(False)
currGen.generate_proposals()


# start the loop to train incrementally
while not completed:
	# generate training data
	mode = 'train'
	isInLoop = True
	totalMeshes = G_NUM_OBJECTS * G_LOOP_MESHES_PER_OBJ
	currGen = Generator(totalMeshes, mode, commonInc.curr_del, isInLoop, k, logFile)
	# False - do not delete existing data
	currGen.generate_data(False)
	currGen.generate_proposals()
	del currGen

	# train using the training data
	latestModel = get_latest_model()
	print ("Going to TRAIN with model initialization %s " % latestModel)
	tr = Trainer(latestModel, logFile)
	# to increase iterations for every loop
	# latestModel = tr.train(G_LOOP_TRAINING_ITERATIONS * train_iter_mul)
	latestModel = tr.train(G_LOOP_TRAINING_ITERATIONS)
	newFile = latestModel.strip('.caffemodel') + '_' + str(k) + '.caffemodel'
	print ("Latest model is: %s trained in: %s iterations" % (newFile, k*G_LOOP_TRAINING_ITERATIONS))
	shutil.copy(latestModel,newFile)
	del latestModel
	del tr
	k += 1

	# Detect using the newly trained model
	testPrototxtFile = G_DETECTOR_PATH + '/models/DefoObject/' + 'test.prototxt'
	testProposalsFile = '/home/isit/workspace/DefoObjInWild/dataset/test_proposals.mat'
	latestModel = get_latest_model()
	print ("Going to init Detector using the model: ", latestModel)
	currDetector = Detector(latestModel, testPrototxtFile, testProposalsFile, logFile)
	predictResMatFile = currDetector.detect()
	gtMatFile = currDetector.get_gt_data()
	del latestModel
	del currDetector

	# Evaluate detection
	currEval = Evaluator(gtMatFile, predictResMatFile, logFile)
	status = currEval.evaluate()
	oldDelta = commonInc.curr_del
	completed, temp_accuracy, meanAvPr = commonInc.check_results()
	print ("MEAN IOU in the iteration is: %s with difficulty: %s " % (meanAvPr, commonInc.curr_del))

	# if we moved from 'no' to 'easy' difficulty, 
	# reset multiplier
	if oldDelta == commonInc.curr_del:
		print ("Incrementing the training iterations multiplier")
		train_iter_mul += 1
	else:
		train_iter_mul = 1

	# append to right array for storing
	if commonInc.curr_del == 'no':
		accuracies.append(temp_accuracy)
		no_map.append(meanAvPr)
	if commonInc.curr_del == 'easy':
		easy_accuracies.append(temp_accuracy)
		easy_map.append(meanAvPr)
	if commonInc.curr_del == 'medium':
		medium_accuracies.append(temp_accuracy)
		medium_map.append(meanAvPr)
	if commonInc.curr_del == 'hard':
		hard_accuracies.append(temp_accuracy)
		hard_map.append(meanAvPr)
	# store the computed accuracies
	sio.savemat('no_accuracy.mat',{'accuracy' : accuracies})
	sio.savemat('easy_accuracy.mat',{'accuracy' : easy_accuracies})
	sio.savemat('medium_accuracy.mat',{'accuracy' : medium_accuracies})
	sio.savemat('hard_accuracy.mat',{'accuracy' : hard_accuracies})
	# store the mean average precisions
	sio.savemat('no_map.mat', {'map' : no_map})
	sio.savemat('easy_map.mat', {'map' : easy_map})
	sio.savemat('medium_map.mat', {'map' : medium_map})
	sio.savemat('hard_map.mat', {'map' : hard_map})