#!/home/isit/anaconda2/bin/python

from global_vars import *
import os


class Evaluator:

	def __init__(self, gt_path, pr_path, scenario, cond, diff, loop_iter, log_file):
		self.gt_path = gt_path
		self.pr_path = pr_path
		self.log_file = log_file
		self.cond = cond
		self.diff = diff
		self.loop_iter = loop_iter
		self.scenario = scenario


	def evaluate( self ):
		os.chdir(G_PROJECT_ROOT)

		matlab_cmd = "addpath(genpath('%s')); evaluate('%s', '%s');" % (G_PROJECT_ROOT, self.gt_path, self.pr_path)
		print ("Going to run Matlab command: ", matlab_cmd)
		try:
			os.system('%s -nodisplay -r "try %s ; catch; end; quit;" %s' % \
				(G_MATLAB_ROOT, matlab_cmd, self.log_file))
		except:
			print ("Problem executing matlab command: %s" % matlab_cmd)
			return False

		return True

	def evaluate_detections(self):
		os.chdir(G_PROJECT_ROOT)

		matlab_cmd = "addpath(genpath('%s')); get_precision_recall('%s', '%s', '%s', '%s', '%s', '%s');" % \
						(G_PROJECT_ROOT, self.gt_path, self.pr_path, self.scenario, self.cond, self.diff, self.loop_iter)
		print ("Going to run Matlab command: ", matlab_cmd)
		try:
			os.system('%s -nodisplay -r "try %s ; catch; end; quit;" %s' % \
				(G_MATLAB_ROOT, matlab_cmd, self.log_file))
		except:
			print ("Problem executing matlab command: %s" % matlab_cmd)
			return False

		return True
